local library = {}

	function library.ftest()
		local po = "poniez"
		wesnoth.message(po)
		return po
	end

	local H = wesnoth.require("lua/helper.lua")
	local IT = wesnoth.require("lua/wml/items.lua")
	local TG =  wesnoth.require("~add-ons/equestria-ununited/lua/wml_tag_gen.lua")

-- TODO
-- refactor
--   move to submodule the implementations of some apis - terrChange, mb abils, Shapes
--   unitStaticValue - handling single and unexisting keys
--   move global filters
-- new access model to WML table
-- improve terrChanges: priorities and queues for colision cases

-- -- --------------- Utils -- -- ---------------
	
	-- converts string enumeration (like "ab,cd,de") into lua array of strings. 
	-- warning: separator must be single-char string. for using character such as "." (dot) escape-prefix must be used (e.g. "%."). see string.find() function info for detais
	function library.enumToArray(enumStr, separator)
		local ret, found_len, previous_ind = {}, 1, 0
		local start_i, end_i = string.find(enumStr, separator)
		while (start_i ~= nil) do			
			ret[found_len] = string.sub(enumStr, previous_ind+1, start_i-1)
			previous_ind = start_i
			start_i, end_i = string.find(enumStr, separator, previous_ind+1)
			found_len = found_len + 1
		end
		ret[found_len] = string.sub(enumStr, previous_ind+1, string.len(enumStr))
		return ret
	end

	-- converts comma-separated enum into array
	function library.enumToArr(enumStr)
		return library.enumToArray(enumStr, ",")
	end

	-- get some value from [unit_type] config. paramPath may be simple string (such as "cost", "race") or extended path (e.g. "attack.damage"). If multiple subtags presented, then first one gonna be used.
	function library.unitStaticValue(unitTypeId, paramPath)
		local cfg = wesnoth.unit_types[unitTypeId].__cfg
		local start_i, dummy = string.find(paramPath, "%.")		
		if start_i == nil then			
			return cfg[paramPath]
		else
			local members = library.enumToArray(paramPath, "%.")
			local next = cfg
			for i=1, #members-1 do
				next=H.get_child(next, members[i])
			end
			--if next==nil then
			--	return nil
			--end
			return next[members[#members]]
		end
	end

	-- check is table a correct wml tag: {"tagname", {"body"}}
	function library.isWmlSubtag(val)
		if type(val)=="table" then
			if type(val[1])=="string" and type(val[2])=="table" then return true end			
		end
		return false
	end

	-- same as helper.child_range but for iterating over any tag name
	function library.allChildRange(cfg)
		local elem = 0
		local elem_max = #cfg
		return function()
			elem = elem +1
			while elem<=elem_max do
				if library.isWmlSubtag(cfg[elem]) then
					return cfg[elem][2]
				end
				elem = elem +1			
			end
		end
	end

	-- extracts 3 substrings. exapmle:
	-- input: "prefix[key=value]", output: "prefix", "key", "value"
	function library.extractKeyValuePart(str)
		local st = string.find(str, "%[")
		--if st == nil then return nil end
		if st == nil then return str end
		local siz = string.len(str)
		local eq = string.find(str, "=")		
		local prefix = string.sub(str, 1, st-1)
		local key = string.sub(str, st+1, eq-1)
		local val = string.sub(str, eq+1, siz-1)
		return prefix, key, val
	end

	-- returns first found child of given WML which having child matches the condition "key=val"
	function library.findSpecificChild(parent_cfg, tag, key, val)
		local tagIterator = nil
		if string.len(tag)==0 then
			tagIterator = function() return library.allChildRange(parent_cfg) end
		else
			tagIterator = function() return H.child_range(parent_cfg, tag) end
		end
		for v in tagIterator() do
			if v[key] == val then
				return v
			end
		end
	end

	-- access to WML table using string path. path may be entered as subtag name or "[key=value]" filter, presented in subtag
	-- example: readWml(unit, "attack[name=knife].specials.poison")
	-- not finished
	-- not optimized
	function library.readWml(cfg, keyPath)
		local start_i, dummy = string.find(keyPath, "%.")
		if start_i == nil then
			local p, k, v = library.extractKeyValuePart(keyPath)
			--if p==nil then return cfg[keyPath] end
			if p==keyPath then return cfg[keyPath] end
			return library.findSpecificChild(cfg, p, k, v)
		end
		local members = library.enumToArray(keyPath, "%.")
		local next = cfg
		for i=1, #members-1 do
			local p, k, v = library.extractKeyValuePart(members[i])
			--if p==nil then
			if p==members[i] then
				next=H.get_child(next, members[i])
			else
				next=library.findSpecificChild(next, p, k, v)
			end
			if next==nil then return nil end
		end
		local last_ind = members[#members]
		local last_m = next[last_ind]
		--library.debugVar(last_m, "LAST_M")
		--library.debugVar(next, "NEXT")
		--library.debugVar(p2, "P")
		--library.debugVar(k2, "K")
		--library.debugVar(members[#members], "mem")
		--library.debugVar(next[4][1], "NEXT_4")
		if last_m==nil then
			local p2, k2, v2 = library.extractKeyValuePart(last_ind)
			if p2==members[last_ind] then
				return H.get_child(next, members[last_ind])
			else
				return library.findSpecificChild(next, p2, k2, v2)
			end

			--library.debugVar(p2, "P")
			--library.debugVar(k2, "K")
			--library.debugVar(v2, "V")
		else
			local p, k, v = library.extractKeyValuePart(last_m)
			if p==last_m then
			--if p==nil then
				if library.isWmlSubtag(last_m) then 
					return last_m[2]
				else
					return last_m
				end
			else
				return library.findSpecificChild(last_m, p, k, v)
			end		
		end
	end

	-- same as "unitStaticValue" but could use additional type of source
	-- src - unit type string, or unit's table/userdata
	function library.quickUnitStaticValue(src, path)
		local t = type(src)
		if t=="string" then
			return library.unitStaticValue(src, path)
		else
			return library.unitStaticValue(src.type, path)
		end
	end

	-- variable increaser for creating some unique ids
	function library.nextId()
		local v = wesnoth.get_variable("LUA_UNIQUE_ID_GEN")
		if (v==nil) then
			wesnoth.set_variable("LUA_UNIQUE_ID_GEN", 1)
			return 1
		else
			v=v+1
			wesnoth.set_variable("LUA_UNIQUE_ID_GEN", v)
			return v
		end
	end

	-- unite 2 tables. if strict searching keys with true values (e.g. and) in both arrays, else append each key (e.g or)
	function library.mergeIntBoolTables(arr1, arr2, strict)
		local fin = {}
		if arr1==nil then return arr2 end
		if arr2==nil then return arr1 end
		local keyset = {}		
		for k,v in pairs(arr1) do
			table.insert(keyset, k)
		end
		for k,v in pairs(arr2) do
			table.insert(keyset, k)
		end
		for i=1, #keyset do
			local k = keyset[i]
			if strict then
				if arr1[k] and arr2[k] then fin[k] = true end
			else
				if arr1[k] or arr2[k] then fin[k] = true end
			end
		end
		return fin
	end

	-- not tested
	-- returns new array with items accepted by filter.
	-- filt_func - function (1 param, arr member) must return boolean (item accepted)
	function library.filterArray(arr, filt_func)
		local out, ii = {}, 1
		for i=1, #arr do
			if filt_func(arr[i]) then
				out[ii] = arr[i]
				ii=ii+1
			end
		end
		return out
	end

	-- search if str_match is presented in str_list enumeration
	function library.presentInList(str_match, str_list, separ)
		local arr = library.enumToArray(str_list, separ)
		for i=1, #arr do
			if str_match == arr[i] then
				return true
			end
		end
		return false
	end

	-- detects if element inside array
	function library.presentInArray(val, arr)
		for i=1, #arr do
			if val==arr[i] then
				return true
			end
		end
		return false
	end

	-- detects if element inside table
	-- return nil if not found, else returns key
	function library.presentInSet(val, arr_set)
		for k,v in pairs(arr_set) do
			if (v == val) then return k end
		end
		return nil
	end

	-- v==0 or v==nil condition
	function library.isNilOrZero(v)
		if v==nil then return true end
		if v==0 then return true end
		return false
	end

	function library.emptyStr(s)
		return (s==nil or s=="")
	end

	-- returns true if coords the same
	function library.samePoint(x1, y1, x2, y2)
		return (x1==x2 and y1==y2)
	end

	-- returns true if coords the same
	function library.sameTriple(x1, y1, z1, x2, y2, z2)
		return (x1==x2 and y1==y2 and z1==z2)
	end

	-- returns true if arr_pnt coordinates is [cx,cy]
	-- arr_pnt may be numeric array ([1] is x and [2] is y) or have named keys (arr_pnt.x, arr_pnt.y)
	function library.isArrPointAt(arr_pnt, cx, cy)
		local ax = arr_pnt.x
		if (ax == nil) then
			local bx, by = arr_pnt[1], arr_pnt[2]
			return library.samePoint(bx,by, cx, cy)
		else
			local ay = arr_pnt.y
			return library.samePoint(ax,ay, cx, cy)
		end
	end

	function library.dummyExtract(v)
		return v
	end

	library.intKeyPrefix = "lua_int_key_"
	library.intKeyPrefixLen = string.len(library.intKeyPrefix)

	-- detects if string starts from intKeyPrefix value and then return the rest of string. else returns nil.
	-- not tested
	function library.detectKeyPrefix(key_str)
		local ind = string.find(key_str, library.intKeyPrefix)
		if ind~=1 then return nil end
		return tonumber(string.sub(key_str,	library.intKeyPrefixLen+1))
	end

	-- transforms string of key=value pairs separated by semicolons(;) into lua table
	-- not tested
	function library.extractParameters(str_src)
		local container = {}
		local kv_pairs = library.enumToArray(str_src, ";")
		for k,v in pairs(kv_pairs) do
			local eq = string.find(v, "=")
			local key = string.sub(v, 1, eq-1)
			local value = string.sub(v, eq+1)
			container[key] = value
		end
		return container
	end

	-- same as previous with export into WML
	function library.extractParametersToWmlVar(str_src, var_name)
		local vv = library.extractParameters(str_src)
		for k,v in pairs(vv) do
			wesnoth.set_variable(var_name.."."..k, v)
		end
	end

	-- convert WML tree into lua table (using int-key replacement)
	-- mb not tested completely
	function library.wmlToLua(obj)
		local ret = {}
		for k,v in pairs(obj) do
			if v~=nil then
				if library.isWmlSubtag(v) then
					local key = library.detectKeyPrefix(v[1])
					if key==nil then key = v[1] end
					ret[key] = library.wmlToLua(v[2])
				else
					local key = library.detectKeyPrefix(k)
					if key==nil then key = k end
					ret[key] = v
				end
			end
		end
		return ret
	end

	-- convert lua table to WML tree (*)
	-- replaces number keys into string ones using [library.intKeyPrefix] prefix
	function library.luaToWml(obj)
		local wml = {}
		for k,v in pairs(obj) do
			local keystr = k
			if type(k)=="number" then
				keystr = library.intKeyPrefix..k
			end
			if type(v)=="table" then
				table.insert(wml, {keystr, library.luaToWml(v)})
			else
				wml[keystr] = v
			end			
		end
		return wml
	end

	-- common tool for acccessing string-key object collections
	function library.strSetIO(table, key_str, object, do_read, exist_behave)
		local ex = table[key_str];
		if do_read then			
			if exist_behave and ex==nil then
				table[key_str] = object
				return object
			end
			return ex;
		else
			if ex~=nil and exist_behave==false then
				return false
			end
			table[key_str] = object
		end
	end

	-- copies all props from parent table to child	
	--	prop_enum - comma-separated list of field names to copy
	-- not tested
	function library.copyParentProperties(parent, child, prop_enum)
		local li = library.enumToArr(prop_enum)
		for i,name in pairs(li) do
			child[name] = parent[name]
		end
	end
-------------------------------------- Common Effects and Payment ---------------------------------------------------------
-- common way to implement some bonus and it's payment

	-- container for functions of custom particular actions (ability effects, etc)
	library.commonEffects = {}
	-- custom parameter container, may be used by some effect functions
	library.commonExecParams = nil

	function library.appendCommonEffectParam(key, value)
		if 	library.commonExecParams == nil then
			library.commonExecParams = {}
		end
		library.commonExecParams[key] = value
	end

		------------------------------ All Particular Lua Common Effects ---------------------------------------

		-- heals primary unit on [hp] amount
		library.commonEffects["EditPrimaryHp"] = function()
			local un = library.getPrimaryUnit()
			un.hitpoints = un.hitpoints + library.commonExecParams.hp
			wesnoth.put_unit(un)
			commonExecParams = nil
		end

		library.commonEffects["TwoBonusMovesForPrayer"] = function()
			local un = library.commonExecParams.prayer
			un.moves = un.moves +2			
		end

		------------------------------ Common Effects Ended ---------------------------------------

	-- check if unit able to provide standard payment. returns [true,nil] or [false,"problem message string"]
	-- not tested
	-- null unsafe
	-- needs subtag access fix
	function library.checkComPayment(params, unit_cfg)
		local ne = "Not enough "
		if params.cost_gold ~= nil then
			if wesnoth.sides[unit_cfg.side]<params.cost_gold then return false, ne.."gold, need "..params.cost_gold end
		end
		if params.cost_xp ~= nil then
			if unit_cfg.experience < params.cost_xp then return false, ne.."experience, need "..params.cost_xp end
		end
		if params.cost_hp ~= nil then
			if unit_cfg.hitpoints < params.cost_hp then return false, ne.."hitpoints, need "..params.cost_hp end
		end
		if params.cost_variables~=nil then
			-- wrong!
			for k,v in pairs(params.cost_variables) do
				if unit_cfg.variables[k]<params.cost_variables[k] then return false end
			end
		end
		return true
	end

	-- not finished
	-- need fix
	-- auto-change unit variables for payment attributes (hp,xp,gold) and [cost_variables],[cost_status] subtags (for customize attributes). args:
		-- params: table container of cost info (cost_hp, cost_xp, cost_gold, etc...)
		-- unit_cfg: table of unit which is gonna pay
		-- post_unit: boolean, call wesnoth.put_unit() in the end or do not (optional)
	function library.commonPayment(params, unit_cfg, post_unit)
		if params.cost_gold ~= nil then
			library.editSideGold(unit_cfg.side, false, params.cost_hp)
		end
		if params.cost_xp ~= nil then
			unit_cfg.experience = unit_cfg.experience - params.cost_xp
		end
		if params.cost_hp ~= nil then
			unit_cfg.hitpoints = unit_cfg.hitpoints - params.cost_hp
		end
		if params.cost_variables~=nil then
			-- wrong: access to child
			for k,v in pairs(params.cost_variables) do
				unit_cfg.variables[k] =  unit_cfg.variables[k] - params.cost_variables[k]
			end
		end
		if params.cost_status~=nil then
			for k,v in pairs(params.cost_status) do
				unit_cfg.status[k] = true
			end
		end
		if post_unit then wesnoth.put_unit(unit_cfg) end
	end

	-- apply common effect. it may be function from "library.commonEffects" or event name. if lua function choosen returns it's result.
	-- params_tag contain fields:
		--	exec_type: "wml"/"lua"
		--	exec_name: function key/event name
		--	exec_params(optional): semicolon-separated pairs
	-- not tested (especially WML part)
	function library.execCommonEffect(params_tag)
		local pars = params_tag.exec_params
		if params_tag.exec_type == "wml" then
			if not library.emptyStr(weap_spec_buff.params) then
				library.commonExecParams = library.extractParametersToWmlVar(pars, "custom_ev_args")
			end
			-- not finished: may add other [fire_event] params passing
			wesnoth.fire("fire_event", {name=params_tag.exec_name, {"primary_unit", {id=library.getPrimaryUnit().id}},{"secondary_unit", {id=library.getSecondUnit().id}}})
			wesnoth.set_variable("custom_ev_args", nil)
		elseif params_tag.exec_type == "lua" then
			if not library.emptyStr(params_tag.exec_params) then
				library.commonExecParams = library.extractParameters(pars)
			end
			local r = library.commonEffects[params_tag.exec_name]()
			library.commonExecParams = nil
			return r
		else
			library.debugVar(params_tag.exec_type, "Wrong param 'exec_type' for execCommonEffect(). It must equal 'wml' or 'lua'")
		end
	end

	-- applies payment and effect
	function library.applyEffectAndPay(pars, unit_payer, post_payed_unit)
		library.commonPayment(pars, unit_payer, post_payed_unit)
		return library.execCommonEffect(pars)
	end

	-- checks payment possibility, applies payment and effect if it's possible or tries to show a problem text else.
	-- returns effect result (may be nil) or false (on payment fail)
	-- most hi-level function for "Common Effects" subsystem for now
	function library.applyEffectIfCanPay(pars, unit_payer, post_payed_unit)
		local can, msg = library.checkComPayment(pars, unit_payer)
		if can then
			return library.applyEffectAndPay(pars, unit_payer, post_payed_unit)
		else
			if msg==nil then return false end
			library.fireMessageCurrSide(msg)
			return false
		end
	end

	-- custom condition check
	-- abstraction not defined
	function library.commonCheckConditions()

	end

-- -- --------------- Sides -- -- ---------------
	
	function library.getCurrentSide()
		local sn = wesnoth.get_variable("side_number")
		return wesnoth.sides[sn]
	end

	function library.setCurrentGold(value, absolute)
		local sd = library.getCurrentSide()
		if absolute then
			sd.gold = value
		else
			sd.gold = sd.gold + value
		end
	end

	function library.editSideGold(sn, absolute, append_gold)
		local sd = wesnoth.sides[sn]
		if absolute then
			sd.gold = append_gold
		else
			sd.gold = sd.gold + append_gold
		end
	end

	function library.enoughGoldFor(g)
		local sd = library.getCurrentSide()
		return (sd.gold >= g)
	end

	-- not tested
	function library.disallowAiRec(typeR)
		local edit = {}
		for k,v in pairs(wesnoth.sides) do
			if v.controller=="ai" then
				table.insert(edit, k)
			end
		end
		if #edit > 0 then
			for i=1, #edit do
				library.disallowRecruit(typeR, edit[i])
			end
		end
	end

	function library.findSideByTeamName(tn)
		for k,v in pairs(wesnoth.sides) do
			if v.team_name==tn then return k end
		end
	end

	-- disallows rectruiting of type
	function library.disallowRecruit(typeR, sid_n)
		local sid = wesnoth.sides[sid_n]
		local filterFunc = function(str)
			if str==typeR then
				return false
			else
				return true
			end
		end
		local withoutR = library.filterArray(sid.recruit, filterFunc)
		local wml_a = {side=sid_n, recruit=library.enumerateUnitsInfo(withoutR, library.dummyExtract, ",")}
		wesnoth.fire("set_recruit", wml_a)
	end

	-- allows rectruiting of type
	-- 
	function library.allowRecruit(typeR, sid_n)
		local sid = wesnoth.sides[sid_n]
		if library.presentInArray(typeR, sid.recruit) then return end
		local enum = library.enumerateUnitsInfo(sid.recruit, library.dummyExtract, ",")
		enum = enum..","..typeR
		wesnoth.fire("set_recruit", {side=sid_n, recruit=enum})
	end

	-- returns current wesnoth state of x1,y1,x2,y2 vars
	function library.getCurrentCoordinates()
		return wesnoth.get_variable("x1"),wesnoth.get_variable("y1"),wesnoth.get_variable("x2"),wesnoth.get_variable("y2")
	end

-- -- --------------- Unit utils -- -- ---------------

	-- return table, unit id
	function library.getPrimaryUnit()
		local uu = wesnoth.get_variable("unit")
		local un_id = uu.id
		return uu, un_id
	end

	-- return table, unit id
	function library.getSecondUnit()
		local uu = wesnoth.get_variable("second_unit")
		local un_id = uu.id
		return uu, un_id
	end

	-- return x,y of $unit
	function library.getPrimaryUnitPos()
		local uu = library.getPrimaryUnit()
		return uu.x, uu.y
	end

	-- return userdata
	function library.getUnitById(uid)
		return wesnoth.get_units({id=uid})[1]
	end

	-- return userdata
	function library.getUnitAt(ax,ay)
		return wesnoth.get_units({x=ax, y=ay})[1]
	end

	-- return userdata
	function library.getPrimaryUnitUserdata()
		local uu = wesnoth.get_variable("unit.id")
		return wesnoth.get_units({ id = uu })[1]
	end

	-- return userdata
	function library.getSecondaryUnitUserdata()
		local uu = wesnoth.get_variable("second_unit.id")
		return wesnoth.get_units({ id = uu })[1]
	end

	-- enumerate ids of all units passed
	function library.listUnitIds(filter)
		local un_arr,out_arr = wesnoth.get_units(filter), {}
		for i=1, #un_arr do
			out_arr[i] = un_arr[i].id
		end
		return out_arr
	end

	-- returns boolean value, is unit passes a filter. unit may be presented as userdata or table (__cfg). warning: second way is only acceptable in case of onboard unit(s)!
	function library.matchUnit(un, filter)
		local t = type(un)
		if t=="userdata" then
			return wesnoth.match_unit(un, filter)
		elseif t=="table" then
			local u_dat = library.getUnitById(un.id)
			return wesnoth.match_unit(u_dat, filter)
		else
			wesnoth.message("Wrong unit form for matchUnit function: "..t)
		end
	end

	-- not tested
	function library.storeUnit(un_cfg)
		
	end

	function library.getUnitWritableTable(filter)
		--return wesnoth.copy_unit(wesnoth.get_units(filter)[1]).__cfg
		local un1 = wesnoth.copy_unit(wesnoth.get_units(filter)[1])
		local un2 = un1.__cfg							
		return un2
	end

	function library.getUnitWritableTab(unit_userdata)
		return wesnoth.copy_unit(unit_userdata).__cfg
	end

	-- executes func for each unit ability. params:
	-- un - unit, table or userdata
	-- visitor - function(ability_table, ability_index), if returns value equals "abort_return" param, then visiting will be aborted
	function library.forEachAbility(un, visitor, abort_return)
		local cfg = library.tryGetUnitTable(un)
		local abk = -1
		for k,v in pairs(cfg) do
			if type(v) == "table" then
				if v[1] == "abilities" then
					abk = k
					break
				end
			end
		end
		if abk~=-1 then
			for k,v in pairs(cfg[abk][2]) do
				-- v struct: {"abil_tag_name", {BODY}}
				local ret = visitor(v, k)
				if ret == abort_return then
					return ret
				end
			end
		end
	end

	-- detect if unit has abil. params: un - table/userdata of unit, abil_name - ability TAG name to look for
	function library.hasAbility(un, abil_name)
		--return (library.unitStaticValue(un.type, "abilities."..abil_name..".id")~=nil)
		local found = false
		local nameCheck = function(abi, index)
			if abi[1] == abil_name then
				found = true
				return "found"
			end
			return nil
		end
		library.forEachAbility(un, nameCheck, "found")
		return found
	end

	-- check if primary unit wears abil
	function library.hasAbilPrimUn(atag)
		local u = library.getPrimaryUnitUserdata()
		return library.hasAbility(u, atag)
	end

	-- returns attacks table(attack body list) and int array of same length, int array strores attack index in unit struct
	function library.getAttacksInfo(unit_table)
		local un2 = unit_table
		local ret, meta, n = {}, {}, 1
		for i=1, #un2 do
			if type(un2[i])=="table" then
				if un2[i][1] == "attack" then
					ret[n] = un2[i][2]
					meta[n] = i
					n=n+1
				end
			end
		end
		return ret, meta
	end

	-- returns bool array which is representing acceptable attack indexes (as keys). accept if "key" param in attack was found and it's value is "value"
	function library.filterAttacks(att_list, key, value)
		local ret, a = {}, 1
		local deb_s = ""
		for i=1, #att_list do
			for k,v in pairs(att_list[i]) do
				if k==key then
					ret[i] = (v==value)					
				end			
			end
		end		
		return ret
	end

	-- not tested
	function library.listAttackSpecials(att_tab, spec_id)
		local spec_arr_ind, rem_index = -1, -1
		for k,v in pairs(att_tab) do
			if type(v)=="table" then
				if v[1]=="specials" then
					spec_arr_ind = i
					break
				end
			end
		end
		if spec_arr_ind~=-1 then
			for k,v in pairs(att_tab[spec_arr_ind][2]) do
				if v.id==spec_id then
					rem_index=k
					break
				end
			end
			if rem_index~=-1 then
				table.remove(att_tab[spec_arr_ind][2], rem_index)
			end
		end
	end

	-- return index of [specials] subtable if any, else returns -1
	function library.getSpecialsBlockIndex(attack_body)
		for i=1, #attack_body do
			if type(attack_body[i])=="table" then
				if attack_body[i][1]=="specials" then
					return i
				end
			end
		end
		return -1
	end

	-- adds given attack special into un_table
	function library.appendAttackSpecial(un_table, attack_index, a_spec)
		local attack = un_table[attack_index][2]
		local spec_arr_ind = library.getSpecialsBlockIndex(attack)
		local specs
		if spec_arr_ind==-1 then
			specs = {"specials", {a_spec}}
			table.insert(un_table[attack_index][2], specs)
			return 1
		else
			specs = attack[spec_arr_ind][2]
			table.insert(specs, a_spec)
			return #specs
		end
	end

	-- not tested
	-- returns attack name which has entered special id
	function library.findAttackNameWithSpecial(unit_cfg, spec_id)
		local attack_arr, att_inds = library.getAttacksInfo(unit_cfg)
		for k,v in pairs(attack_arr) do
			local specs_i = library.getSpecialsBlockIndex(v)
			if specs_i ~= -1 then
				local specs = v[specs_i][2]
				if not library.emptyArray(specs) then					
					for i=1, #specs do
						if specs[i][2].id == spec_id then
							return v.name
						end
					end
				end
			end			
		end
	end

	-- removes 1st found attack special with specified id
	-- not tested
	function library.removeAttackSpecial(un_table, id_str)
		local attacks, indexes = library.getAttacksInfo(un_table)
		local f_ind = -1
		for i,a in pairs(attacks) do
			if a.id==id_str then
				f_ind = i
				break
			end
		end
		if f_ind~=-1 then
			local att_ind = indexes[f_ind]
			local att_i, specs_i, spec_i = library.findAttackIndexBySpecialId(attacks, id_str)
			if att_i ~= nil then
				local attack_index = indexes[att_i]
				table.remove(un_table[attack_index][specs_i], spec_i)
			end
		end
	end

	-- adds	a poison special to all blade and pierce attacks
	function library.addPoisonToAttacks(filter)
		local u_tab = library.getUnitWritableTable(filter)
		local atts, inds = library.getAttacksInfo(u_tab)
		local att_p = library.filterAttacks(atts, "type", "pierce")
		local att_b = library.filterAttacks(atts, "type", "blade")
		local att_both = {}
		if att_p==nil and att_b==nil then
			wesnoth.message("both null")
			return
		end
		att_both=library.mergeIntBoolTables(att_p, att_b, false)		
		-- unit_att_inds's key - ordinal num, value - index of proper attack in the unit table:	
		local unit_att_inds, a = {}, 1
		for k,v in pairs(att_both) do
			if v then
				unit_att_inds[a]=inds[k]
				a=a+1
			end
		end
		local a_spec = {"poison", {name="poison", id="new_pois"}}
		for i=1, #unit_att_inds do
			library.appendAttackSpecial(u_tab, unit_att_inds[i], a_spec)			
		end
		wesnoth.put_unit(u_tab)
	end

	-- produces a string containing info about each unit from input array. PARAMS:
	-- units - units array
	-- func_extract - function with 1 param (unit) which returns some string info about it
	-- separator - char to separate items in result string
	function library.enumerateUnitsInfo(units, func_extract, separator)
		if (#units == 0) then
			return
		end
		local ret = ''
		for i=1, #units do
			local str = func_extract(units[i])
			ret = ret..str
			if i<#units then
				ret=ret..separator
			end
		end
		return ret
	end

	-- get table from unit userdata, or keep the same, if input is already a table
	function library.tryGetUnitTable(un)
		local cf = un.__cfg
		if cf==nil then
			cf = un
		end
		return cf
	end

	-- return index of [status] tag. param un_cfg - table
	function library.findUnitStatusKey(un_cfg)
		local st_key = nil
		for k,v in pairs(un_cfg) do
			if type(v)=="table" then
				if v[1]=="status" then
					st_key = k
					break
				end
			end
		end
		return st_key		
	end

	-- read unit status
	function library.getUnitStatus(un, key)
		local cf = library.tryGetUnitTable(un)
		local st_key = library.findUnitStatusKey(cf)
		if st_key~=nil then
			local r = cf[st_key][2][key]
			return (r==true)
		end
	end

	-- edit unit's status, keys such as poisoned,slowed
	-- param "un" must be userdata unit
	function library.setUnitStatus(un, key, newVal)
		local u2 = wesnoth.copy_unit(un).__cfg
		local st_key = library.findUnitStatusKey(u2)
		if st_key~=nil then
			u2[st_key][2][key] = newVal
			wesnoth.put_unit(u2)
		end
	end

	-- return primary unit's ability active (bool)
	-- input - tag name of abil
	function library.getUnitAbilState(un, ability_tag)
		return wesnoth.unit_ability(un, ability_tag)
	end

	function library.getHealthPercent(un)
		local max, curr = un.max_hitpoints, un.hitpoints
		return math.ceil(curr/(max/100))
	end

	-- not tested
	function library.restoreUnitTurn(un)		
		if un~=nil then
			local c = library.tryGetUnitTable(un)
			c.moves = c.max_moves
			c.attacks = c.max_attacks
			wesnoth.put_unit(un)
		else
			-- prim
		end
	end

	-- not tested
	-- returns list of all enemies who are currently hidden by any kind of [hides] ability. if no any returns nil. PARAMS:
		-- sideSeekFor : int, side who is searching
		-- outputFormat : string, one of [userdata/table/id/pair/xy]
	function library.listHiddenEnemies(sideSeekFor, outputFormat)
		local filter = { 
			{"filter_vision", {visible="no", side=sideSeekFor}}, {"filter_side",{ {"has_enemy",{side=sideSeekFor}} }}
		}
		local units = wesnoth.get_units(filter)
		if units==nil then return nil end
		if #units==0 then return nil end
		local ret = {}
		for i=1, #units do
			if wesnoth.unit_ability(units[i], "hides") then
				local of, append = outputFormat, nil
				if outputFormat == nil then of = "xy" end
				if of=="userdata" then
					append = units[i]
				elseif of=="table" then
					append = units[i].__cfg
				elseif of=="id" then
					append = units[i].__cfg.id
				elseif of=="pair" then
					append = {units[i].__cfg.x, units[i].__cfg.y}
				elseif of=="xy" then
					append = {x=units[i].__cfg.x, y=units[i].__cfg.y}
				else
					wesnoth.message("Unknown 'outputFormat' for listHiddenEnemies().")
					return nil
				end
				table.insert(ret, append)
			end
		end
		if #ret>0 then
			return ret
		else
			return nil
		end
	end

	function library.getPrimaryAndAbil(abil_tag)
		local pu = library.getPrimaryUnit()
		local at = library.readWml(pu, "abilities."..abil_tag)
		return pu, at
	end

	-- not tested
	function library.getAdjacentUnitWithAbil(abil_id, adj_to_id)
		local au = wesnoth.get_units({ ability=abil_id, {"filter_adjacent", {id=adj_to_id}} })[1].__cfg
		local abil = library.readWml(au, "abilities.[id="..abil_id.."]")
		return au, abil
	end

	-- set unit at new pos. params:
	--		un_data - unit userdata entry
	--		new_x,new_y - new pos
	-- not safe: error on busy cell
	function library.moveUnit(un_data, new_x, new_y)
		local cf = un_data.__cfg
		cf.x, cf.y = new_x, new_y
		wesnoth.extract_unit(un_data)
		wesnoth.put_unit(cf)
	end

-- -- --------------- Abilities (including wpn-specs) exec -- -- -------------------------------------------------------

	-- finds all current side units with "greed" ability and exec for each unit:
	-- if xp=0 lose health (ability.value)
	-- if xp>0 lose 1 xp point with chance (ability.chance)
	function library.abilGreed()
		local units = wesnoth.get_units({side=wesnoth.get_variable("side_number"), ability="greed"})
		if units == nil then return end
		if #units == 0 then return end
		for i,un in pairs(units) do
			local ch = library.unitStaticValue(un.type, "abilities.greed.chance")
			local u = library.getUnitWritableTab(un)
			if u.experience > 0 then
				local bool = library.randomBool(ch)
				if (bool) then
					u.experience = u.experience -1
					wesnoth.put_unit(u)
				end
			else
				local hp_lose = library.unitStaticValue(un.type, "abilities.greed.value")
				wesnoth.fire("harm_unit", { amount=hp_lose, {"filter", {id = u.id}} })
			end
		end
	end

	-- add health to secondary unit (ability.value - percent of max hp)
	function library.abilBloodthirsty()
		local killer = library.getSecondUnit()
		local bonus_coef = library.unitStaticValue(killer.type, "abilities.bloodthirsty.value")/100
		local hp_bonus = math.ceil(killer.max_hitpoints*bonus_coef)
		wesnoth.fire("heal_unit", { amount=hp_bonus, {"filter", {id=killer.id}} })
	end

	-- create unit of specified type	
	function library.abilReproduce()
		local adv = library.getPrimaryUnit()
		local u_type = library.unitStaticValue(adv.type, "abilities.reproduce.type")
		wesnoth.fire("unit", {type=u_type, x=adv.x, y=adv.y})
	end

	-- ?????
	function library.abilBecomeThing()
		local thu = library.getPrimaryUnit()
		local uu = wesnoth.get_units({id = thu.id})[1]
		local abil_active = library.getUnitAbilState(uu, "become_thing")
		if abil_active then
			
		end
	end

	-- not tested
	function library.abilBlind()
		local attacker, attacked = library.getPrimaryUnit(), library.getSecondUnit()
		local att_name = library.findAttackNameWithSpecial(attacker, "blinds")
		-- error:
		if att_name==nil then return end
		local spec_node = library.readWml(attacker, "attack[name="..att_name.."].specials.[id=blinds]")
		local val, abs = spec_node.value, spec_node.absolute
		if abs==false then
			val = H.round(attacked.max_moves/100*val)
		end
		local vr = library.readWml(attacked, "variables")
		vr["max_moves_old"] = attacked.max_moves
		attacked.max_moves = attacked.max_moves - val
		local st = library.readWml(attacked, "status")
		st["blinded"] = true
		wesnoth.put_unit(attacked)
	end

	-- containers of functions	
	--library.castEffects = {}

	--library.castEffects["CAST_CREATE_THING"] = function()		
		--library.putThingAt(library.unitCaster.x, library.unitCaster.y, library.readWml(library.unitCaster,"attacks.specials."))		
	--end

	--library.castEffects["CAST_CREATE_UNIT"] = function()
	--	local ux, uy = library.unitCaster.x, library.unitCaster.y
	--end

	--library.castEffects["CAST_FIRE_MISSILE"] = function()
	--	
	--end

	--library.castEffects["CAST_HEAL_SELF"] = function()
	--	
	--end

	-- optional args from cast-on-attack wepon special
	library.castParams = nil
	-- just buffer - unit which cast current spell
	library.unitCaster = nil

	-- exec some effect on attacking
	-- effect limit not implemented
	-- not refactored
	function library.abilCastOnAttack(ev_name)
		local attack_role, hited, weap_spec_buff = nil, nil, nil
		library.unitCaster = library.getPrimaryUnit()
		if string.find(ev_name, "attacker") == 1 then
			attack_role = true
			weap_spec_buff = library.readWml(library.unitCaster, "attack.specials.cast_on_attack")
		else
			weap_spec_buff = library.readWml(library.getSecondUnit(), "attack.specials.cast_on_attack")
		end
		if string.find(ev_name, "hits") ~= nil then
			hited = true
		end
		-- do nothing if rolling chance fails
		if  not library.randomBool(weap_spec_buff.chance) then
			return nil
		end
		-- do nothing if actual event params not same with conditions declared inside weapon special tag
		if (attack_role and weap_spec_buff.role=="defend") or
		   (attack_role==false and weap_spec_buff.role=="attack") or
		   (hited and weap_spec_buff.on=="miss") or
		   (hited==false and weap_spec_buff.on=="hit") then
			return nil
		end
		if weap_spec_buff.exec_type=="wml" then
			if not library.emptyStr(weap_spec_buff.params) then 
				library.castParams = library.extractParametersToWmlVar(weap_spec_buff.params, "custom_ev_args")
			end
			wesnoth.fire("fire_event", {name=weap_spec_buff.exec_name, {"primary_unit", {id=library.getPrimaryUnit().id}},{"secondary_unit", {id=library.getSecondUnit().id}}})
		else
			if not library.emptyStr(weap_spec_buff.params) then 
				library.castParams = library.extractParameters(weap_spec_buff.params)
			end
			library.castEffects[weap_spec_buff.exec_name]()
		end
		library.unitCaster = nil
	end

	function library.abilCursing()
		
	end

	-- executes on start turn
	--not finished
	function library.abilMoveEx(uid)
		local un_cfg = nil
		--un_cfg.x = 
	end

	--
	function library.abilMoveExInit()
	end

	-- transforming terrain on x1,y1 cell ("terrain_trail" ability)
	function library.abilTrailSet()		
		local x1, y1 = library.getCurrentCoordinates()
		local unt = library.getPrimaryUnit()
		local unt_trail = library.readWml(unt, "abilities.terrain_trail")
		local terr_accept = wesnoth.match_location(x1, y1, {terrain=unt_trail.type_from})
		if terr_accept then
			library.terrChange(x1,y1, "trail", unt_trail.type_to, unt_trail.duration, true)
		end		
	end

	-- restore transformed terrains ("terrain_trail" ability)
	function library.abilTrailRestore()
		local restoreList = {}
		local terr_visitor = function(ax, ay, val)
			if val.id=="trail" then
				if val.type==0 then
					table.insert(restoreList, {x=ax,y=ay})
				else
					val.type = val.type -1
				end
			end			
		end
		library.terrChangeVisit(terr_visitor)
		if #restoreList == 0 then return end
		for i=1, #restoreList do
			library.terrChangeRestore(restoreList[i].x, restoreList[i].y)
		end
	end

	-- checks conditions and then do payment and pray reward (effect) if accepted.
	-- primary unit is prayer, single adjacent to it must be idol praying to
	-- not tested
	function library.abilPray()
		local prayerUnit = library.getPrimaryUnit()
		local idolUnit = wesnoth.get_units({ ability="pray_idol", {"filter_adjacent", {id=prayerUnit.id}} })[1].__cfg
		local abilTag = library.readWml(idolUnit, "abilities.pray_idol")
		local enough = library.checkComPayment(abilTag, library.getPrimaryUnit())
		if enough then
			library.appendCommonEffectParam("prayer", prayerUnit)
			local filterTag = library.readWml(idolUnit, "abilities.pray_idol.prayer_filter")
			if filterTag~=nil then
				if library.matchUnit(prayerUnit, filterTag) then
					library.applyEffectAndPay(abilTag, prayerUnit)
				else
					library.fireMessageCurrSide("This unit isn't able to pray to such idol.")
				end
			else
				library.applyEffectAndPay(abilTag, prayerUnit)
			end
			wesnoth.put_unit(prayerUnit)
			library.commonExecParams = nil
		else
			library.fireMessageCurrSide("Unit have not prepared enough for praying now.")
		end
	end

	-- creates few copies of unit (visions) for specified price
	function library.abilVisUnitCreate(count, price)
		if library.enoughGoldFor(price) then
			library.setCurrentGold(-price, false)
			local protoUd = wesnoth.get_units({id=library.visionTmp.pid})[1]
			wesnoth.extract_unit(protoUd)
			local visions = {}
			for i=1,count do
				local vis = wesnoth.copy_unit(protoUd)
				local vis_cf = vis.__cfg
				vis_cf.role = "vision"
				vis_cf.id = vis_cf.id..i
				local vis_vars = H.get_child(vis_cf, "variables")
				--library.readWml(vis_cf, "variables")
				if vis_vars==nil then
					
				end
				vis_vars.vision_source = library.visionTmp.cid
				visions[i] = vis
			end
			local putUnitNear = function(nx,ny, u_put)
				local cx, cy = wesnoth.find_vacant_tile(nx, ny, u_put)
				u_put.x,u_put.y = cx,cy
				wesnoth.put_unit(u_put)
				return cx,cy
			end
			local realInd = math.random(count) +1
			table.insert(visions, realInd, protoUd)
			local rx, ry = nil, nil
			for k,v in pairs(visions) do
				local cx, cy = putUnitNear(library.visionTmp.cx, library.visionTmp.cy, v)
				if k==realInd then
					rx,ry = cx,cy
				end
			end
			library.setLabel({x=rx, y=ry, team_name=library.getCurrentSide().team_name, visible_in_fog="no", text="REAL"})
			-- also need label-removing stuff
		else
			library.fireMessageCurrSide("No enough gold")
		end
		library.visionTmp = nil
	end

	-- launch parameter menu and create new vision units
	-- not tested
	function library.abilUnitVisionsNew()
		local protoUnit = library.getPrimaryUnit();
		local visCreator, abilTag = library.getAdjacentUnitWithAbil("vision_unit_creator", protoUnit.id)
		local cnt_list, pr_list, choose_str, param_str = library.enumToArr(abilTag.counts), library.enumToArr(abilTag.prices), "", ""
		for i=1,#cnt_list do
			choose_str = choose_str..cnt_list[i].." visions for "..pr_list[i].."gold"
			param_str = param_str..cnt_list[i]..","..pr_list[i]
			if i<#cnt_list then
				choose_str = choose_str..","
				param_str = param_str..";"
			end
		end
		library.visionTmp = {}
		library.visionTmp.pid = protoUnit.id
		library.visionTmp.cx = visCreator.x
		library.visionTmp.cy = visCreator.y
		library.visionTmp.cid = visCreator.id
		library.fireMenuLuaCall("How many visions u want to appear?", choose_str, "commonlib.abilVisUnitCreate", param_str, true)
	end

	-- highlight visions of selected creator
	-- not tested
	function library.abilUnitVisionsHighlight()
		
	end
	
-- -- --------------- Thing utils -- -- ---------------

	-- read thing data
	function library.getProductNamePrice(thingNameStr)
		local thing_name = wesnoth.get_variable(thingNameStr..".text")
		local thing_price = wesnoth.get_variable(thingNameStr..".price")
		return thing_name, thing_price
	end

	-- effects of some thing types may be processed via lua
	function library.thingEffect(action, thingVarName)
		if action=='drop' then			
		elseif action=='pick' then
		--elseif action=='activate' then
		--elseif action=='disable' then
		--else
		end
	end

	function library.thingEffectXY()
	end

	-- compose string with product name and price
	function library.getProductStr(thingNameStr)
		local thing_name, thing_price = library.getProductNamePrice(thingNameStr)
		return thing_name.." ["..thing_price.." gold]"
	end

	-- returns WML var name where thing stored
	function library.getPlaceVarname(x,y)
		return "HEX_"..x.."_"..y
	end

	-- returns thing ID situated on x,y
	function library.getThingAt(x,y)
		local val = wesnoth.get_variable(library.getPlaceVarname(x,y))
		return val
	end

	-- returns thing name (displayable) situated on x,y
	function library.getThingNameAt(x,y)
		local val = library.getThingAt(x,y)
		if val==nil then return nil end
		return wesnoth.get_variable(val..".text")
	end

	-- returns picture path of thing
	function library.getThingIcon(th_name)
		return wesnoth.get_variable(th_name..".icon")
	end

	-- create thing on gameboard
	function library.putThingAt(x,y, tng)
		local v = library.getPlaceVarname(x,y)
		wesnoth.set_variable(v, tng)
		IT.place_image(x, y, library.getThingIcon(tng))
	end
	
	-- destroy thing on gameboard
	function library.delThingAt(x,y)
		local v = library.getPlaceVarname(x,y)
		local t_name = wesnoth.get_variable(library.getPlaceVarname(x,y))
		local img = library.getThingIcon(t_name)
		wesnoth.set_variable(v, nil)
		IT.remove(x, y, img)
	end

	-- returns bool, is unit have specified thing
	-- not tested
	function library.unitCarryThing(un, thing)
		local u_th = un.variables.thing
		if library.emptyStr(u_th) then return false end
		return (thing == u_th)
	end

-- -- --------------- Craft utils -- -- ---------------

	-- returns filtered list of crafters who already started
	function library.listActiveCrafterIds(sideNum)
		local units = wesnoth.get_units({side=sideNum, ability="crafts"})
		local accept, a = {}, 1
		for i=1, #units do
			if units[i].variables.crafting_now then
				accept[a] = units[i].id
				a = a+1
			end
		end
		return accept
	end

	-- must born (by wesnoth.fire()) [fire_event] tags which check/update status of each crafter
	function library.processCraft(sideNum)
		listIds = library.listActiveCrafterIds(sideNum)
		for i=1,#listIds do
			wesnoth.message("process crafter id:"..listIds[i])
			wesnoth.fire("fire_event", {name="crafting_process", {"primary_unit", {id=listIds[i]}}})
		end
	end

	-- ask crafting confirmation. if "start" chosen, then var_out=0, "cancel" does var_out=1
	-- var_out - wesnoth variable name
	function library.fireCraftConfirm(var_out)
		local pr = library.getPrimaryUnit()
		if (pr == nil) then
			library.warn("Craft confirmation failed.")
			return
		end
		local thing = library.unitStaticValue(pr.type, "abilities.crafts.thing_type")
		local dur = library.unitStaticValue(pr.type, "abilities.crafts.turns")
		local tn, tp = library.getProductNamePrice(thing)
		local ask = pr.name.." could craft ["..tn.."] for "..tp.." gold. Producing gonna last "..dur.." turns (unit will be inactive for that time). Continue?"
		library.fireCommonMenu(ask, "Start crafting,Cancel", var_out)
	end

-- -- --------------- Trading utils -- -- ---------------

	-- [command][lua] -> fireTraderSellOut()
	function library.makeSellOutWmlCall(t)
		return library.makeCmdLuaCall("commonlib.fireTraderSellOut('"..t.."')")
	end

	-- fire trader's selling list
	function library.fireTraderSelling()
		local unt, un_id = library.getPrimaryUnit()
		local sell_str = library.unitStaticValue(unt.type, "abilities.trades.things_sell")
		if (sell_str == nil) then
			wesnoth.message("Trader has no products to sell.")
			return			
		end
		local sell_list = library.enumToArr(sell_str)
		local msg_opts, ind_last = {}, #sell_list+1
		if ind_last == 1 then
			wesnoth.message("Error: trader is void or has no products.")
			return
		end
		for i=1, ind_last do
			-- iterate over things and build optionns with cmds. apply cancel option in the end:
			if (i < ind_last) then
				local thing_name, thing_price = library.getProductNamePrice(sell_list[i])
				local caption = library.getProductStr(sell_list[i])
				msg_opts[i] = library.makeOptionWithCmd(caption, library.makeSellOutWmlCall(sell_list[i]))
			else
				msg_opts[i] = library.makeOptionWithCmd("Cancel", {"command", {}})
			end
		end
		wesnoth.fire("message", library.makeMessageBodyCurrSide("Buy", msg_opts))
	end

	-- called by wml command after selecting thing in menu
	function library.fireTraderSellOut(thing)
		if thing ~= nil then
			local unt, un_id = library.getPrimaryUnit()
			local nn, pp = library.getProductNamePrice(thing)
			local go = library.getCurrentSide().gold
			if go < pp then
				wesnoth.message("No enough gold")
			else
				library.putThingAt(unt.x, unt.y, thing)
				library.setCurrentGold(-pp, false)
			end
		else
			library.warn("Trying sell out a nil thing.")
		end
	end

	function library.fireTraderBuy(ind)
		--local unt, un_id = library.getPrimaryUnit()
	end

-- -- --------------- Recruit limitation -- -- ---------------

	-- table^2 storing limits. top-level: key- side id, value- subtable
	-- sub-level: key- unit type id, value.lim- limit, value.now- alive count
	library.unitLimits = {}

	function library.limitUnits(type_str, length, side)
		local sid = side
		if (side == nil) then
			sid = wesnoth.get_variable("side_number")
		end
		library.unitLimits[sid][type_str].lim = length		
	end

-- -- --------------- Game Board (map/terrain utils) -- -- ---------------

	-- return array of neighbour uit ids
	function library.listAdjUnitIds(unitX, unitY)
		local adjList = listAdjUnits(unitX, unitY)
		result = {}
		for i=1, #adjList do
			result[i] = adjList[i].id
		end
	end

	-- list neighbour units
	-- return userdata array
	function library.listAdjUnits(unitX, unitY)
		return wesnoth.get_units({{"filter_adjacent", {x=unitX, y=unitY}}});	
	end

	-- return array of neighboor points for [px,py]. each point of result is subarray, where [1] is X and [2] is Y.
	function library.listAdjLocations(px,py)
		return wesnoth.get_locations({{"filter_adjacent_location", {x=px, y=py} }})		
	end

	-- list neighbour units of current side
	-- return userdata array
	function library.listAdjUnitsCurrSide(unitX, unitY)
		local c_side = wesnoth.current.side
		return wesnoth.get_units({{"filter_adjacent", {x=unitX, y=unitY, side=c_side}}});
	end

	function library.listAdjThings(cx, cy, includeCell)
		
	end

	-- direction letters (keys), enumerated in clockwise order
	library.directionList = {"N","NE","SE","S","SW","NW"}
	library.directionsSmall = {"n","ne","se","s","sw","nw"}

	-- adjacent coordinates calculating function sets. Composed becuz next(adjacent) cell coordinates are depending on this cell coords is even or odd.
	local dirFirstEven = {["NE"] = function(x,y) return x+1,y end,
	["SE"] = function(x,y) return x+1,y+1 end,
	["NW"] = function(x,y) return x-1,y end,
	["SW"] = function(x,y) return x-1,y+1 end}
	local dirFirstOdd = {["NE"] = function(x,y) return x+1,y-1 end,
	["SE"] = function(x,y) return x+1,y end,
	["NW"] = function(x,y) return x-1,y-1 end,
	["SW"] = function(x,y) return x-1,y end}

	-- return neighbour cell coordinates(x,y - int) for specified side(round direction). inX,inY - initial cell; sideTo - direction key string ("SW" for example)
	function library.whatAdjacentXY(inX, inY, sideTo)
		if sideTo == "_" then
			return inX, inY
		elseif sideTo == "N" then
			return inX, inY-1
		elseif sideTo == "S" then
			return inX, inY+1
		elseif inX % 2 == 0 then
			return dirFirstEven[sideTo](inX, inY)
		else
			return dirFirstOdd[sideTo](inX, inY)
		end
	end
	
	-- returns two direction keys. They are clockwise and oposite relative directions to initial key.
	-- example: calling prevAndNextDirections("N") will return "NW","NE" pair
	function library.prevAndNextDirections(indir)
		inpos = 0
		for i = 1,6,1
		do
			if library.directionList[i] == indir then
				inpos = i
				break
			end
		end
		prev_d = inpos-1
		if prev_d == 0 then prev_d=6 end
		next_d = inpos+1
		if next_d == 7 then next_d=1 end
		return library.directionList[prev_d], library.directionList[next_d]
	end

	-- WML vars used for args and return in following functions (deprecated things)
	-- WML i/o vars: "in_x", "in_y", "in_direction" -> input data; "out_x", "out_y" -> return data
	-- Just calling of whatAdjacentXY() with WML data transfer
	function library.calculateAdjacent()
		input_x = wesnoth.get_variable("in_x")
		input_y = wesnoth.get_variable("in_y")
		inp_direct = wesnoth.get_variable("in_direction")
		outp_x, outp_y = library.whatAdjacentXY(input_x, input_y, inp_direct)
		wesnoth.set_variable("out_x", outp_x)
		wesnoth.set_variable("out_y", outp_y)
	end

	-- WML i/o vars: "in_{x,y,direction}" -> input data; "out_{x,y}_{1,2,3}" -> return data
	-- input: cell and direction. output: 3 cells. 1 - adjacent from initial direction, 2,3 - next and prev cells if turn first cell clockwise and back arount initial cell.
	function library.calculateCornerTriple()
		input_x = wesnoth.get_variable("in_x")
		input_y = wesnoth.get_variable("in_y")
		inp_direct = wesnoth.get_variable("in_direction")
		prev_dir, next_dir = library.prevAndNextDirections(inp_direct)
		xx1, yy1 = library.whatAdjacentXY(input_x, input_y, inp_direct)
		xx2, yy2 = library.whatAdjacentXY(input_x, input_y, prev_dir)
		xx3, yy3 = library.whatAdjacentXY(input_x, input_y, next_dir)
		wesnoth.set_variable("out_x_1", xx1)
		wesnoth.set_variable("out_y_1", yy1)
		wesnoth.set_variable("out_x_2", xx2)
		wesnoth.set_variable("out_y_2", yy2)
		wesnoth.set_variable("out_x_3", xx3)
		wesnoth.set_variable("out_y_3", yy3)
	end

	-- returns array [1-n] of adjacent coords. each item is table with {x,y} keys
	function library.listCellsAround(unx,uny)
		local t_list = {}
		for i=1, #library.directionList do
			t_list[i] = {}
			t_list[i].x, t_list[i].y = library.whatAdjacentXY(unx, uny, library.directionList[i])
		end
		return t_list
	end
	
	-- not tested
	-- returns location set with cells reached by Flood Fill. Cell accepting calculated by check_func(x,y) filter
	function library.listAllFloodFillGroup(x,y, check_func)
		local recurse_append = function(xx,yy,ch_fun,loc_set)
			if loc_set:get(xx,yy)==nil then
				local acc = ch_fun(xx,yy)
				if not acc then
					loc_set:insert(xx,yy,"no")
				else
					loc_set:insert(xx,yy,"yes")
					local neigh = library.listCellsAround(xx,yy)
					for i=1, #neigh do
						recurse_append(neigh[i].x, neigh[i].y, ch_fun, loc_set)
					end
				end
			end
		end
		local ret = location_set.create()
		recurse_append(x,y,check_func,ret)
		return ret:filter(library.buildLocationSetValueFilter("yes"))
	end

	-- filter for location_set which passes every exact value of location
	function library.buildLocationSetValueFilter(pass_value)
		return function(x,y,val)
			return (val==pass_value)
		end
	end

	-- returns loc. set difference: only unique items from loc1
	function library.locationSetDifference(loc1, loc2)
		local it = function(x,y,z)
			return (not loc2:get(x,y))
		end
		return loc1:filter(it);
	end

	-- detects point is inside game board
	function library.inMapBounds(x,y)
		local w,h = wesnoth.get_map_size()
		if x<1 or x>w or y<1 or y>h then return false end
		return true
	end

	-- array of tables about labels was put
	library.labelsList = {}

	-- fires WML [label] action (putting text on board) and remembers this action in labelsList array. some keys are: x,y,text,team_name,visible_in_fog,category,color
	function library.setLabel(args, remember)
		local tagBody = {}
		for k,v in pairs(args) do
			tagBody[k] = v
		end
		wesnoth.fire("label",tagBody)
		if remember then
			-- tagBody.turn = 
			return table.insert(library.labelsList, tagBody)
		end
	end

	-- sets wesnoth label on cell
	function library.eraseLabel(lx, ly)
		library.setLabel({x=lx, y=ly, text=""}, false)
	end

	-- ????
	function library.removeLabels(func_filter, only_one)
		local rm = {}
		for k,v in pairs(library.labelsList) do
			if func_filter(v) then
				table.insert(rm, k)
				if only_one then
					break
				end
			end
		end
		for i=1,#rm do
			local l = labelsList[rm[i]]
			library.setLabel({x=l.x, y=l.y, text=""}, false)
			labelsList[rm[i]] = nil
		end
	end

	-- create non-bidirectioal tunnel (teleport) and and return generated tunnel id
	-- not tested
	function library.createTunnelAutoId(fx, fy, tx, ty, filter)
		local num_id = "auto_tele"..library.nextId()
		local tun_tag = TG.makeTunnelBodyEx(fx, fy, tx, ty, filter, false, num_id)
		wesnoth.fire("tunnel", tun_tag)
		return num_id
	end

	-- find index of lowercase direction string
	function library.detectDirectionIndex(dir)	
		for i=1,6 do
			if dir==library.directionsSmall[i] then
				return i
			end
		end
		return nil
	end

-- -- --------------- Common shape API -------------------------------------------------------

	-- common shapes (terrain groups) <key> - string name, 
	--	<val> - table, keys are string directions (in lower case) with "_" as separators in sequence. 
	--	example proper key values are: "ne", "n_nw", "se_se_n", "_". single-separator means the same point, such as "n_s"
	-- 		values may be any type; default - boolean
	library.shapeAreas = {}

	-- remember transitional values, keys - direction string, values - table:
	--	keys - int (angle)
	--	values - rotated string
	library.cache_rotations = {}

	-- switch
	library.cache_rotations_active = true

	-- -- --------------- Data I/O ---------------

	-- register and return new shape with some value for each node, do nothing and raise warning if key exists
	--		s_name - string name
	--		direction_list - table, array of string with proper directions. each table value (item) will become shape key
	--		copy_key - bool, if true, direction_list keys becomes a values, if false, all values will be 'true'
	function library.addNewShape(s_name, direction_list, copy_key)
		if library.shapeAreas[s_name] ~= nil then
			library.warn("Tried to add shape <"..s_name.."> which key is already exists.")
			return nil
		end
		local ns = library.newShape(direction_list, copy_key)
		library.shapeAreas[s_name] = ns
		return ns
	end

	-- create and return new shape object
	--		direction_list - table, array of string with proper directions. each table value (item) will become shape key
	--		copy_key - bool, if true, direction_list keys becomes a values, if false, all values will be 'true'
	function library.newShape(direction_list, copy_key)
		local ns = {}
		for k,v in pairs(direction_list) do
			local val = true
			if copy_key then
				val = k
			end
			ns[v] = val
		end		
		return ns
	end

	-- copy and return shape with new name
	-- not tested
	function library.copyShape(s_name, new_name)
		local sh = library.shapeAreas.s_name
		if sh==nil then
			library.warn("Tried to copy unexisting shape <"..s_name..">.")
			return nil
		end
		if new_name == s_name then
			library.warn("Tried to copy shape <"..s_name.."> with same name.")
			return nil
		end
		local nsh = {}
		for k,v in pairs(sh) do
			nsh[k] = v
		end
		library.shapeAreas[new_name] = nsh
		return nsh
	end

	-- cahce rotation value, params:
	--	initial - string path, angle - int, final - translated (rotated) path
	-- not tested
	function library.rotationCacheAdd(initial, angle, final)
		local rt = library.cache_rotations[initial]
		local ang
		if rt==nil then
			rt = {}
			library.cache_rotations[initial] = rt
			rt[angle] = final
		else
			ang = rt[angle]
			if ang==nil then
				rt[angle] = final
			end
		end
	end

	-- read and return cached rotation (or nil). params:
	--	dir_str - string (initial), r_a - int angle
	-- not tested
	function library.rotationFromCache(dir_str, r_a)
		local rt = library.cache_rotations[dir_str]
		if rt==nil then return nil end
		return rt[r_a]
	end

	-- -- --------------- Shape utils ---------------

	-- detect if given direction if single-step
	function library.simpleShapeDirection(sd)
		if sd=="_" then return true end
		if library.presentInSet(sd, library.directionsSmall) then return true end
		return false
	end

	-- visit all shape items
	--	shape - proper shape object 
	--	visitor - function(key, val): bool, where key - string direction, val - binded value, if return false, may abort
	--	do_abort - bool, if false, never aborting
	function library.forEachShapeItem(shape, visitor, do_abort)
		for k,v in pairs(shape) do
			local a = visitor(k, v)
			if a and do_abort then
				return
			end
		end
	end

	-- visit all direction elements ordinary (from left to right),
	--	dir - proper string direction, separated by "_", may be upper-case
	--	visitor - function(par) : bool, where par - sigle item, such  as "n". if return false and 'do_abort==true', visiting will be aborted.
	--	do_abort - bool, if false, never aborting
	function library.visitDirectionItems(dir, visitor, do_abort)
		if library.simpleShapeDirection(dir) then
			return visitor(dir)
		end		
		local steps = library.enumToArray(dir, "_")
		local ret = nil
		for i=1, #steps do
			ret = visitor(steps[i])
			if not ret then
				if do_abort then 
					break
				end
			end
		end
		return ret
	end

	-- returns x,y of cell relative on given point
	--		node_key_str - proper direction sequence
	--		base_x, base_y - base point calculating from
	function library.getShapeNodePos(node_key_str, base_x, base_y)
		if node_key_str=="_" then
			return base_x, base_y
		end
		local simple_par = library.presentInSet(node_key_str, library.directionsSmall)
		local node_up = string.upper(node_key_str)
		if simple_par then
			return library.whatAdjacentXY(base_x, base_y, node_up)
		else
			local steps = library.enumToArray(node_up, "_")
			local cx, cy = base_x, base_y
			for i=1, #steps do
				cx, cy = library.whatAdjacentXY(cx, cy, steps[i])
			end
			return cx, cy
		end
	end

	-- return true if the (last) point of direction 'direct' from [bx,by] is [px,py]
	--	px,py - point to check
	--	direct - string direction to try to reach [px,py]
	--	bx,by - base point, direction starts from there
	-- not tested
	function library.pointInDirection(px,py, direct, bx,by)
		local nx,ny = library.getShapeNodePos(direct, bx,by)
		return library.samePoint(px,py,nx,ny)
	end

	-- detect is point [px,py] reachable in shape, if the base point of that shape were [base_x,base_y]
	-- return nil if it's not, else return direction string, which reached to [px,py]
	-- not tested
	function library.pointInShape(px, py, shape, base_x, base_y)
		local found = nil
		local vis = function(key, val)
			local in_dir = library.pointInDirection(px,py, key, base_x, base_y)
			if in_dir then
				found = key
				return false
			end
		end
		library.forEachShapeItem(shape, vis, true)
		return found
	end

	-- returns direction str or index rotated given direction 1 time
	--		dir - direction as starting point, param type depending on given integerMode: if it's true, lower case string (n..nw) else integer direction index (1..6)
	--		clockwise - boolean, which side rotate to
	--		integerMode - mode, boolean, if true, returns integer (1..6) which is index of [library.directionsSmall] array and takes dir as 1..6 integer, else returns string direction and takes string param
	-- not tested
	function library.rotateDirection(dir, clockwise, integerMode)
		if dir=="_" or dir==0 then
			return dir
		end
		local ind = nil		
		if integerMode then
			ind = dir
		else
			ind = library.detectDirectionIndex(dir)
		end
		if ind<1 or ind>6 then
			library.warn("Tried to traslate unacceptable direction <"..dir..">.")
			return
		end
		if clockwise then
			ind = ind +1
			if ind >= 7 then ind=1 end
		else
			ind = ind -1
			if ind <=0 then ind=6 end
		end
		if integerMode then
			return ind
		else
			return library.directionsSmall[ind]
		end
	end

	-- returns new direction string as rounded given dir_str
	--		dir_str : one of 6 lower-case directions (n,s,ne,ne,nw,sw) or "_", which means center point
	--		side : integer, how many times to rotate on 60* clockwise; example: value -2 means -120*, e.g. rotate 120 degrees counter-clockwise
	-- not tested
	function library.translateDirection(dir_str, side)
		if dir_str=="_" then return dir_str end
		if side==0 then return dir_str end
		local s_mod, s_sign, rotated = math.abs(side), true, library.detectDirectionIndex(dir_str)
		if side<0 then s_sign = false end
		for i=1, s_mod do
			-- rotating in integer mode for speed, rotated is number
			rotated = library.rotateDirection(rotated, s_sign, true)
		end
		return library.directionsSmall[rotated]
	end

	-- returns rotated sequence or direction
	-- not tested (cache)
	function library.translateDirectionSequence(dir_str, side)
		local simple_par = library.simpleShapeDirection(dir_str)
		if simple_par then
			return library.translateDirection(dir_str, side)
		end
		local cached	
		if library.cache_rotations_active then
			cached = library.rotationFromCache(dir_str, side)
			if cached~=nil then return cached end
		end
		local steps = library.enumToArray(dir_str, "_")
		local rotated_list = {}
		for i=1, #steps do
			rotated_list[i] = library.translateDirection(steps[i], side)
		end
		local str = ""
		for i=1, #steps do
			str = str.."_"..rotated_list[i]
		end
		str = string.sub(str, 2)
		if library.cache_rotations_active then
			library.rotationCacheAdd(dir_str, side, str)
		end
		return str
	end

	-- returns copy of shape rotated on 60*r degrees
	--	sh - shape object
	--	r - rotate angle, int
	--	copy_val - if false, all shape values of result will be replaced to boolean true's
	-- not tested
	function library.shapeRotated(sh, r, copy_val)
		local ns = {}
		for k,v in pairs(sh) do
			local seq = library.translateDirectionSequence(k, r)
			local v2 = true
			if copy_val then v2 = v end
			ns[seq] = v2
		end
		return ns
	end

	-- returns copy of shape where keys keeped the same, but values replaced to tables {x,y} with actual coords depending on base point [px,py]
	-- not tested
	function library.shapeActualPositions(sh, px, py)
		local news = {}
		local vis = function(key, val)
			local nx,ny = library.getShapeNodePos(key, px,py)
			news[key] = {x=nx, y=ny}
		end
		library.forEachShapeItem(sh, vis, false)
		return news
	end

	--function library.isNeighbours(node1, node2)

	function library.highlightArea(loc, image)
	end


	-- -- --------------- Shape initializing ---------------

	-- inits common shape samples
	function library.initDefaultShapes()
		library.newBoolShape("FullAdjacent", {"n","s","ne","nw","sw","se"})
		library.newBoolShape("SemiAdjacentN", {"n","sw","se"})
		library.newBoolShape("SemiAdjacentS", {"s","ne","nw"})
		-- not tested
		library.newBoolShape("AdjacentRing1", {"n_n","n_nw","n_ne","nw_nw","nw_sw","ne_ne","ne_se","sw_sw","sw_s","s_s","s_se","se_se"})
		library.newBoolShape("AdjacentRing2", {"n_n_n","n_nw_n","n_ne_n","nw_nw_n","nw_nw_nw","nw_sw_nw","nw_sw_sw","ne_ne_n","ne_ne_ne","ne_se_ne","ne_se_se","sw_sw_sw","sw_sw_s","s_s_s","s_s_se","s_s_sw"})
	end

-- -- ---------------/ shape api end ---------------




--------------------------------------- Terrain Changes ---------------------------------------------------

-- common mechanism for board cell replacement.

	-- table with string key like "X_Y" and table-values with stored terr changes info
	-- value-table keys: 
		--terrain: old terrain (the only key required)
		--id: string (optinal)		
		--type: cutom string label such as [short, long, request] for future particular usage (optinal)
	library.changedTerrs = {}
	
-- checks presence of changing entry
function library.terrWasChanged(x,y)
	return (library.changedTerrs[x.."_"..y] ~= nil)
end

-- returns entry or nil
function library.terrChangedData(x,y)	
	return library.changedTerrs[x.."_"..y]
end

-- creates new entry, replaces terrain, and returns changes entry
--params:
	-- changeType: label string
	-- newTerr: terr code
	-- tid: string id
	-- replace - bool, rewrite if exists
function library.terrChange(x,y, tid, newTerr, changeType, replace)
	local terr = library.terrChangedData(x,y)
	if terr==nil then 
		terr = {}
		local ctKey = x.."_"..y;
		library.changedTerrs[ctKey] = terr
	else
		if replace==false then return terr end
	end
	terr.id, terr.terrain, terr.type = tid, wesnoth.get_terrain(x,y), changeType
	wesnoth.set_terrain(x,y, newTerr)
	return terr
end

-- restores terrain and removes changing entry
function library.terrChangeRestore(x,y)
	local tc = library.terrChangedData(x,y)
	if tc==nil then return end
	wesnoth.set_terrain(x,y, tc.terrain)
	library.changedTerrs[x.."_"..y] = nil
end

--function library.listFreeCellsAround(cx, cy)

-- apply terr changing by function(n). on calling param will be empty table (for new) or existing entry.
function library.terrChangeEx(x,y, change_func)
end

-- same as terrChange but less params
function library.terrChangeShort(x,y,id,newTerr)
	return library.terrChange(x,y, id, newTerr, nil, true);
end

-- calls function for each changed terrain entry.
-- vis_func must have 3 params: x,y: (coords), item: (stored terr.change table)
function library.terrChangeVisit(vis_func)
	for k,v in pairs(library.changedTerrs) do
		local ax, ay = library.splitKeyValue(k, "_")
		vis_func(ax, ay, v)
	end
end

-- restore all keys confirmed by filter (when it is returning true)
-- filter_func - function(x,y, entry) - must return boolean; when return true, cell must be restored
-- not tested
function library.terrChangeRestoreByFilter(filter_func)
	local res_list = {}
	local visitor = function (vx, vy, v_ent)
		local acc = filter_func(vx, vy, v_ent)
		if acc then
			table.insert(res_list, {x=vx, y=vy})
		end
	end
	library.terrChangeVisit(visitor)
	for k,v in pairs(res_list) do
		library.terrChangeRestore(v.x, v.y)
	end
end

-- -- --------------- strings: --------------------------------
			
	-- params: un-unit, booleans - info deatils
	-- output: unit info as string
	function library.unitToStr(un, showPos, showHp, showExp)
		local r = un.name
		if showPos then
			r=r.." at ["..un.x..";"..un.y.."]"
		end
		if showHp then
			r=r.." ("..un.hitpoints.."/"..un.max_hitpoints.." hp)"
		end
		if showExp then
			r=r.." ("..un.experience.."/"..un.max_experience.." xp)"
		end
		return r
	end

	function library.sizeOf(table)
		return #table
	end

	-- returns 2 strings that was splited by single-char separator in input string
	function library.splitKeyValue(str, sep_char)
		local eq = string.find(str, sep_char)
		local key = string.sub(str, 1, eq-1)
		local value = string.sub(str, eq+1)
		return key, value
	end
-- -- --------------- Return WML strings (compatibility-save copies. wml_tag_gen.lua functions must be used/added directly in the future code): 

	library.makeEvent = TG.makeEvent
	library.makeCmdFireEvent = TG.makeCmdFireEvent
	library.makeCmdLuaCall = TG.makeCmdLuaCall
	library.makeOptionWithCmd = TG.makeOptionWithCmd
	library.makeSetVarCmd = TG.makeSetVarCmd
	library.makeCancelOption = TG.makeCancelOption
	library.makeLuaCmd = TG.makeLuaCmd
	library.makeLuaCmdWithParam = TG.makeLuaCmdWithParam
	library.makeMessageBodyCurrSide = TG.makeMessageBodyCurrSide
	library.makeLuaCallOption = TG.makeLuaCallOption
	library.makeClearVarCmd = TG.makeClearVarCmd

-- -- --------------- Wesnoth array edit -- -- ---------------

	function library.copyArray(arr)
		if arr == nil then
			return nil
		end
		local ret = {}
		for i=1, #arr do
			ret[i] = arr[i]
		end
		return ret
	end

	function library.emptyArray(ar)
		if ar==nil then 
			return true 
		else
			if #ar==0 then return true end
		end
		return false
	end

	-- is location [lx,ly] contained in loc_array pairs. loc_array = table of pair-tables with 2 values, such as wesnoth.get_locations() returns
	-- not tested
	function library.includedLocation(lx,ly, loc_array)
		if library.emptyArray(loc_array) then return false end
		for k,v in pairs(loc_array) do
			if v[1] == lx and v[2] == ly then
				return true
			end
		end
		return false
	end

-- not tested yet.

	function library.removeArrayItem(varname, remindex, move_last)
		local arr = wesnoth.get_variable(varname)
		local siz = #arr
		if siz == 0 then
			return 0
		end
		if move_last then
			arr[remindex] = arr[siz]
			arr[siz] = nil
		end
		wesnoth.set_variable(varname, arr)
		return #arr
	end


	function library.addArrayItem(arrayvar, itemvar, clearthen)
		local arr = wesnoth.get_variable(arrayvar)
		local siz = #arr
		arr[siz+1] = wesnoth.get_variable(itemvar);
		if clearthen then
			wesnoth.set_variable(itemvar, nil)
		end
		wesnoth.set_variable(arrayvar, arr)
	end

-- -- --------------- Random things -- -- ---------------

	-- returns random Bool, input chance means propability of TRUE result. This chance must be integer percentage value between 0..100 bounds
	function library.randomBool(chance)
		if chance>=100 then
			return true
		elseif chance<=0 then
			return false
		else
			local val = wesnoth.random(100)
			return (val <= chance)
		end
	end

-- -- --------------- UI activities -- -- ---------------
	
	-- fires [message] menu and writes selected item index (zero-based) into WML var. PARAMS:
		-- textAsk - message text
		-- textAnswers - variants to select (comma-separated string) 
		-- outVarName - wesnoth variable to output selected index
	function library.fireCommonMenu(textAsk, textAnswers, outVarName)
		local options, textArray = {}, library.enumToArr(textAnswers)
		for i=1, #textArray do
			local cmd = library.makeSetVarCmd(outVarName, i-1)
			options[i]=library.makeOptionWithCmd(textArray[i], cmd)
		end
		local msg=library.makeMessageBodyCurrSide(textAsk, options)
		wesnoth.fire("message", msg)
	end

	-- fires [message] menu with calling specified lua function for each option. PARAMS:
		-- textAsk - message
		-- textAnswers - variants to select (comma-separated string)
		-- answerCalls - string of function calls (options separated by semicolon ";", NOT by ","). must have same element count as textAnswers
		-- appendCancel - bool, do add dummy "Cancel" option
	function library.fireMenuLuaCalls(textAsk, textAnswers, answerCalls, appendCancel)
		local opts, calls = library.enumToArr(textAnswers), library.enumToArray(answerCalls, ';')
		local options = {}
		for i=1, #opts do
			options[i] = library.makeLuaCallOption(opts[i], calls[i])
		end
		if appendCancel then
			options[#opts+1] = library.makeCancelOption()
		end
		wesnoth.fire("message", library.makeMessageBodyCurrSide(textAsk, options))
	end

	-- almost the same as "fireMenuLuaCalls" but for calling same function with different args for each option:
		-- answerCallName - string, function name (without brackets)
		-- answerCallPars - param list string: "," separates param values, ";" separates option bounds. example value: "1,1;1,2;2,1;2,2" (function with 2 args)
	function library.fireMenuLuaCall(textAsk, textAnswers, answerCallName, answerCallPars, appendCancel)
		local answerCalls = ""
		local pars = library.enumToArray(answerCallPars, ';')
		for i=1, #pars do
			answerCalls = answerCalls..answerCallName.."("..pars[i]..")"
			if i<#pars then 
				answerCalls = answerCalls..";"
			end
		end
		library.fireMenuLuaCalls(textAsk, textAnswers, answerCalls, appendCancel)
	end

	function library.fireMessageCurrSide(text)
		local msg=library.makeMessageBodyCurrSide(text, nil)
		wesnoth.fire("message", msg)
	end

	-- fire unit selection dialog. selecting option will call lua function with some args. PARAMS:
		-- unit_list - array of units select from 
		-- msg_text - head label
		-- call_str - function name (call for ui feedback) 
		-- func_get_label - function (1 param (unit), return string) which calculates text label about unit. 
		-- func_get_params - function (1 param (unit), return string) to get param(s) for "call_str" function.
	function library.fireMenuUnitSel(unit_list, msg_text, call_str, func_get_label, func_get_params)
		local texts = library.enumerateUnitsInfo(unit_list, func_get_label, ",")
		local params = library.enumerateUnitsInfo(unit_list, func_get_params, ";")
		library.fireMenuLuaCall(msg_text, texts, call_str, params, true)
	end

-- -- --------------- Debug, Testing, info -- -- ---------------

	function library.warn(txt)
		wesnoth.message("[Warning!]  "..txt)
	end

	function library.debugVar(val, startText)
		local show, st = nil, nil
		if startText==nil then
			st=""
		else
			st=startText.." "
		end		
		if val~=nil then
			local t = type(val)
			if	t=='table' then
				show = t.." ("..#val..")"
			elseif	t=='userdata' or t=='function' or t=='nil' then
				show = t		
			elseif t=="boolean" then
				if val then
					show="true"
				else
					show="false"
				end
			else			
				show = "["..t.."] "..val			
			end
		else
			wesnoth.message(st.." nil")
			return
		end
		wesnoth.message(st..show)
	end

	function library.tableToString(o)
		if type(o) == 'table' then
		    local s = '{'
		    for k,v in pairs(o) do
		            if type(k) ~= 'number' then k = '"'..k..'"' end
		            s = s .. '['..k..']='..library.tableToString(v)..','
		    end
			local siz = string.len(s)
			s = string.sub(s, 1, siz-1)
		    return s .. '}'
		else
		    return tostring(o)
		end
	end

	function library.showTable(t)
		library.fireMessageCurrSide(library.tableToString(t))
	end

	function library.debugInterruptable(str)
		wesnoth.fire("message", {speaker="narrator", message=str, side_for=wesnoth.current.side})
	end

	function library.showShape(s, x,y, terr)
		local s2 = library.shapeActualPositions(s, x, y)
		for k,v in pairs(s2) do
			library.terrChangeShort(v.x,v.y,"shapetest",terr)
		end
	end

	library.testTerrs = {"Ko","Iwr","Aa^Emf"}
	library.tt_index = 1

	function library.test2()
		local x1,y1 = library.getCurrentCoordinates()
		local resf = function(x,y,sh_ent)
			return (sh_ent.id=="shapetest")
		end
		local sh = nil
		if library.tt_index==1 then
			sh = library.addNewShape("test2", {"_","n","n_n","n_n_ne"}, false)
			library.showShape(sh, x1,y1, library.testTerrs[library.tt_index])
		else
			sh = library.shapeAreas["test2"]
			library.terrChangeRestoreByFilter(resf)
			if library.tt_index==2 then
				local sh2 = library.shapeRotated(sh, 2, false)
				library.showShape(sh2, x1,y1, library.testTerrs[library.tt_index])
			elseif library.tt_index==3 then
				local sh2 = library.shapeRotated(sh, -1, false)
				library.showShape(sh2, x1,y1, library.testTerrs[library.tt_index])
			end
		end
		library.tt_index = library.tt_index+1
	end


-- -- ------------------------------- GLOBAL functions -- -- -------------------------------

-- -- --------------- Filters -- -- ---------------
-- (for using with lua_function key inside StandardUnitFilter)

	-- mb need side verify (unfinished)
	function enoughMoneyForTurningBox(a_unit)
		local gg = library.enoughGoldFor(13)
		return gg
	end

	function enoughHealthPercentForBox(un)
		local pc = library.getHealthPercent(un)
		return (pc >= 80)
	end

	function acceptableUnitForSacrefice(un)
		local cfg = library.tryGetUnitTable(un)
		local st = library.getUnitStatus(cfg, "sacrefice")
		if st == true then
			return false
		end
		local attack_name = library.findAttackNameWithSpecial(cfg, "sacrefice")
		--local attacks, att_inds = library.getAttacksInfo(cfg)
		--local at_i, spb_i, sp_i = library.findAttackIndexBySpecialId(attacks, "sacrefice")
		if attack_name==nil then			
			return false
		end
		local need = library.readWml(cfg, "attack[name="..attack_name.."].specials.[id=sacrefice].value")
		local have = cfg.hitpoints
		return (have>need)
	end

				-- for detect trading distance
				function needCheckTraderDistance(a_unit)
					local val = commonlib.unitStaticValue(a_unit.type, "abilities.trades.distance")
					return not (val == true)
				end
				
				function enoughMoneyForStartCrafting(a_unit)
					local need = commonlib.unitStaticValue(a_unit.type, "abilities.crafts.cost")
					local have = commonlib.getCurrentSide().gold
					return (have >= need)
				end

				function acceptableTerrainForCraft(a_unit)
					local ter = commonlib.unitStaticValue(a_unit.type, "abilities.crafts.terrain")
					return wesnoth.match_location(a_unit.x, a_unit.y, {terrain=ter})
				end

				function checkWeaponsPoisonable(a_un)
					local a_unit = a_un.__cfg
					for i=1, #a_unit do
						local mem = a_unit[i]
						if type(mem) == table then
							wesnoth.message(mem[1])
						end						
					end
					return false
				end

				function checkBecomeThingActive(uni)
					if commonlib.hasAbility(uni, "become_thing") then
						return commonlib.getUnitAbilState(uni, "become_thing")
					else
						return false
					end
				end

-- -- ----------------- -- ----------------- -- ----------------- -- ---------------
return library