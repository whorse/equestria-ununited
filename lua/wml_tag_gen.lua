-- -- --------------- Return WML strings (tag builders): --------------------------------

local library = {}

	--[event]name=
	function library.makeEvent(evName)
		return {"event", {name=evName, first_time_only=no}}
	end

	--[command][fire_event]name=
	function library.makeCmdFireEvent(evName)
		return {"command", {
			{"fire_event", {["name"]=evName}}
		}}
	end

	--[lua]command=
	function library.makeCmdLuaCall(funcNameWithArgs) 
		return {"command", {
			{"lua", {code=funcNameWithArgs}}
		}}
	end

	--[option]text=
	function library.makeOptionWithCmd(text, cmdStruct)
		return {"option", {message=text, cmdStruct}}
	end

	--[command][set_variable]
	function library.makeSetVarCmd(varname, setval)
		return {"command", {
			{"set_variable", {name=varname, value=setval}}
		}}
	end

	--[command][clear_variable]
	function library.makeClearVarCmd(varname)
		return {"command", {
			{"clear_variable", {name=varname}}
		}}
	end

	--[option]message="Cancel"[command]
	function library.makeCancelOption()
		local cmd = {"command", {}}
		return library.makeOptionWithCmd("(Cancel)", cmd)
	end
	

	--[command][lua]code=src	
	function library.makeLuaCmd(src)
		return {"command", {
					{"lua", {code=src}}
		}}
	end

	--[command][lua]code="funcName(param)"
	function library.makeLuaCmdWithParam(funcName, param)
		local lua_str = funcName.."("..param..")"
		return makeLuaCmd(lua_str)
	end

	--[message][option](opt_i)
	-- fix options passing
	function library.makeMessageBodyCurrSide(text, options)
		local m = {speaker="narrator", message=text, side_for=wesnoth.current.side}
		if options == nil then return m end
		for i=1, #options do
			m[i] = options[i]
		end
		return m
	end	

	--[option][lua]message=txt,code="fullCall"
	function library.makeLuaCallOption(txt, fullCall)
		local opt = {"option", {
			message=txt,
				{"command", {
					{"lua", {code=fullCall}}
				}}
			}
		}	
		return opt
	end

	-- not tested
	-- [tunnel][source(x,y),target(x,y),filter, bidirectional, tunnel_id]
	function library.makeTunnelBodyEx(fx,fy, tx,ty, filt, bidi, tid)
		local o = {
			{"source", {x=fx, y=fy}},
			{"target", {x=tx, y=ty}},
			{"filter", filt},
			bidirectional = bidi,
			id = tid
		}
		return o
	end

	-- [filter_adjacent_location] {cond_body}
	-- seems wrong
	function library.makeFilterAdjacentCondition(cond_body)
		local f = {
			{"filter_adjacent_location", cond_body}
		}
		return f
	end

	-- [filter] x1,y1 [or]x2,y2 [or]x3,y3 (...)
	--	points - array of tables with {x,y} keys
	function library.makeMultiplePointsFilter(points)
		local f = { x=points[1].x, y=points[1].y }
		local max = #points
		if max<2 then return f end
		for i=2, max do
			local sub_or = {"or", { x=points[i].x, y=points[i].y }}
			table.insert(f, sub_or)
		end
		return f
	end

return library;
