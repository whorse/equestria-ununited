local library = {}

local common = wesnoth.require("~add-ons/equestria-ununited/lua/common.lua")
local tag_gen = wesnoth.require("~add-ons/equestria-ununited/lua/wml_tag_gen.lua")

-- TODO: terrain matches to WML var, and using it for menu filters
--		unit properties influence on jump skills
--		testing limitation ways

	-- exceptional rules array
	-- <values> : table
	--		x,y - int, position
	--		height - assign height score
	-- not used yet
	library.heightExcepts = {}

	-- cell height rules; <key>	- int, <val> - {base (bool), pattern (string), value (int)}
	library.heightTable = {}

	-- adjacent influence on height; <key> - str pattern, <val> - int
	library.adjacentScores = {}

	-- settings and constraints
	library.options = {}

	-- biggest score difference which is allowing jump from lower to higher point
	library.options.maxJumpDiff = 2

	-- cache flag
	library.options.useCache = true

	-- conditions for proper src and dest points	
	library.options.jumpTerrainMatch = "!,Qx*,X*,Ql*,Mv"

	-- terrain types for jumping over them
	library.options.terrainJumpOver = "Qx*"

	-- assigned height function(x,y)
	library.cellHeightFormula = nil

	-- assigned filter-creating function(fx,fy, tx,ty)	
	library.unitFilterGenerator = nil

	-- array[x][y] of int, saved height results for quick access
	library.cacheBuf = {}

-- -- --------------- Data I/O --------------------------------

	-- append new pattern to heightTable
	function library.addTerrainPattern(pat, base, val)
		local ob = {}
		ob.value = val
		ob.base  = base
		ob.pattern = pat
		table.insert(library.heightTable, ob)
	end

	-- returns nil if there's no height except on [cx,cy] cell, else returns it's exceptional value
	function library.findExceptHeight(cx,cy)
		for k,v in pairs(library.heightExcepts) do
			if v.x==cx and v.y==cy then
				return v.height
			end
		end
		return nil
	end

-- -- --------------- Data Initialize --------------------------------

	-- default rules about heights
	function library.initDefaultTerrHeight()
		library.addTerrainPattern("H*",true,6)
		library.addTerrainPattern("F*",true,3)
		library.addTerrainPattern("M*",true,9)
		library.addTerrainPattern("C*,K*",true,1)
		library.addTerrainPattern("Ko*,Ket*",false,1)
		library.addTerrainPattern("*^F*",false,3)
	end

	-- default rules about adjacent influence on heights
	function library.initAdjacentWeights()
		library.adjacentScores["M*"] = 4
		library.adjacentScores["H*,F*"] = 3
		library.adjacentScores["C*,K*,Uh*"] = 2
		library.adjacentScores["R*,G*,I*"] = 1
		library.adjacentScores["U*"] = -1
	end

	-- use constraint height for some terrains instead of calculated by functions
	function library.setExcept(ex, ey, h)
		table.insert(library.heightExcepts, {x=ex, y=ey, height=h})
	end

	-- preparing cfg
	function library.initAll()
		library.initDefaultTerrHeight()
		library.initAdjacentWeights()
		library.cellHeightFormula = library.heigthFormula1
		library.unitFilterGenerator = library.filterBuilder1		
	end

-- -- --------------- Interface --------------------------------

	function library.showHeight()
		local x,y = wesnoth.get_variable("x1"), wesnoth.get_variable("y1")
		local h = library.cellHeightFormula(x,y)
		common.fireMessageCurrSide("["..x..","..y.."] cell height is "..h)
	end

-- -- --------------- Implementation variants --------------------------------


	-- returns score number for particular terrain depends on current heightTable config
	function library.getCellHeightScore(x,y)
		local base_inited, base_v, plus_v = false, 0, 0
		for k,v in pairs(library.heightTable) do
			local terr_accept = wesnoth.match_location(x, y, {terrain=v.pattern})
			if (terr_accept) then
				if v.base then
					if base_inited then common.warn("Pattern bases match multiple cells!") end
					base_inited = true;
					base_v = v.value
				else
					plus_v = plus_v + v.value
				end
			end
		end
		return base_v + plus_v
	end

	-- returns score number depending on adjacent cells
	function library.getCellHeightAdjacentScore(px,py)
		local nb = wesnoth.get_locations({{"filter_adjacent_location", {x=px, y=py} }})
		local siz, score = #nb, 0
		for i=1,siz do
			local cx, cy = nb[i][1], nb[i][2]
			for k,v in pairs(library.adjacentScores) do
				local acc = wesnoth.match_location(cx,cy, {terrain=k})
				if acc then
					score = score + v
				end
			end
		end
		return score/siz
	end

	-- default device, calculates height as sum of terrain and mean adjacent scores or returns exception value (if it presenting), also uses the cache when options flag activated
	function library.heigthFormula1(x,y)
		local exc = library.findExceptHeight(x,y)
		if exc ~= nil then
			return exc
		end
		local need_save = false
		if library.options.useCache then
			local col = library.cacheBuf[x]
			if col == nil then
				library.cacheBuf[x] = {}
				need_save = true
			else
				local c_val = col[y]
				if c_val == nil then
					need_save = true
				else
					return c_val
				end
			end
		end
		local base, adj = library.getCellHeightScore(x,y), library.getCellHeightAdjacentScore(x,y)
		local h = base + adj
		if need_save then
			library.cacheBuf[x][y] = h
		end
		return h
	end

	library.unitFilter1 = { formula="$this_unit.max_moves > 4 and $this_unit.moves < 4" }
	
	library.dummyFilter = {}

	-- default device
	function library.filterBuilder1(fx,fy, tx,ty)
		return library.unitFilter1
	end

-- -- --------------- Invariant stuff --------------------------------
	
	-- check if cells are neighbours
	function library.isAdjacentPoints(fx,fy, tx,ty)
		return wesnoth.match_location(fx,fy, {{"filter_adjacent_location", {x=tx, y=ty} }})
	end

	-- adds jump entry if unique
	function library.appendJumpIfUnique(fr_x, fr_y, to_x, to_y, list)
		for k,v in pairs(list) do
			if v.fx==fr_x and v.fy==fr_y and v.tx==to_x and v.ty==to_y then
				return
			end
		end
		table.insert(list, {fx=fr_x, fy=fr_y, tx=to_x, ty=to_y})
	end

	-- appends to list new table(s) {fx,fy, tx,ty} which represents acceptable jump ways
	function library.extractJumpWays(xa, ya, xb, yb, list)
		local h_a, h_b = library.cellHeightFormula(xa, ya), library.cellHeightFormula(xb, yb)
		local diff = math.abs(h_a - h_b)
		if diff <= library.options.maxJumpDiff then
			-- both sides
			library.appendJumpIfUnique(xa, ya, xb, yb, list)
			library.appendJumpIfUnique(xb, yb, xa, ya, list)
		else
			-- single side
			if h_a > h_b then
				library.appendJumpIfUnique(xa, ya, xb, yb, list)
			else
				library.appendJumpIfUnique(xb, yb, xa, ya, list)
			end
		end
	end

	-- append all unique jump ways into jump_points
	--	bx,by - byss to jumping over
	--	jump_points = array of {fx,fy,tx,ty} table elements to collect result
	function library.collectJumpWaysOf(bx, by, jump_points)
		local properAdj = wesnoth.get_locations({ terrain=library.options.jumpTerrainMatch, {"filter_adjacent_location", {x=bx, y=by}}  })
		if common.emptyArray(properAdj) then
			return
		end
		for i=1, #properAdj do
			for j=1, #properAdj do
				if i~=j then
					local ix, iy, jx, jy = properAdj[i][1], properAdj[i][2], properAdj[j][1], properAdj[j][2]
					if not library.isAdjacentPoints(ix, iy, jx, jy) then
						library.extractJumpWays(ix, iy, jx, jy, jump_points)
					end
				end
			end
		end
	end

	-- returns string from jump coordinates
	-- 		jw : table with {fx,fy,tx,ty} integers
	function library.buildTunnelId(jw)
		return "jump_from_"..jw.fx.."_"..jw.fy.."_to_"..jw.tx.."_"..jw.ty
	end

	-- executes [tunnel] WML to create jumping way
	-- 		jw : table with {fx,fy,tx,ty} integers
	function library.fireJumpTunnel(jw)
		--local src_filt = {x=jw.fx, y=jw.fy}
		--local dest_filt = {x=jw.tx, y=jw.ty}
		local uni_filter = library.unitFilterGenerator(jw.fx,jw.fy, jw.tx,jw.ty)
		local tunnel_body = tag_gen.makeTunnelBodyEx(jw.fx,jw.fy, jw.tx,jw.ty, uni_filter, false, library.buildTunnelId(jw))
		wesnoth.fire("tunnel", tunnel_body)
	end

	-- create all the teleports near abyss terrains
	function library.createJumpingTeleports()
		local byss_list = wesnoth.get_locations({terrain = library.options.terrainJumpOver})
		--common.debugVar(#byss_list, "bysses length")
		local jump_ways = {}
		for k,v in pairs(byss_list) do
			library.collectJumpWaysOf(v[1], v[2], jump_ways)
		end
		--common.debugVar(#jump_ways, "jump ways collected")
		for k,v in pairs(jump_ways) do
			library.fireJumpTunnel(v)
		end
	end


return library