local library = {}

local common = wesnoth.require("~add-ons/equestria-ununited/lua/common.lua")

-- TODO:
--	finish switch-images and sounds
--	handle terrChange clashes (add changes checking)
--	cache for movement events for speed [lp]
--	add spam-preventing limits


-- ========================= Doors and Switches =========================


	-- table of door objects, <key> - string id
	-- values:
	--	nodes - array
	--		x,y - absolute pos
	--		terr - terrain on CLOSED door
	--	closed - boolean
	--	key - string
	--	usable - boolean, could open/close via menu when standing adjacent
	--	on_busy - function(door_entry, unit) : boolean; if true, door may be closed
	--	optional: filter(un),
	library.doors = {}

	-- const, as terr-change type key
	library.DOOR_CHANGE_TYPE = "door"

	-- on_busy behaviour variants
	library.doorBusyFuncs = {}

	-- table of button entries, <key> - string id
	-- values:
	--	x,y - pos
	--	actions: table, possible keys: [menu, enter_hex, exit_hex] (up to 3 items max), 
	--		values: 
	--			filter (optional), 
	--			exec (required) - function(switch_entry) : boolean; if returned true, need do invert switch state
	--			text, description (optional, "menu" item only) - strings, menu title and additional help info
	--	door_id (optional) - string, door key, for those which providing door control
	--	appereance (optional yet) - string, appereance key for graphics
	--	state (optional) - bool, used for door-switches, must equal 'door.closed'
	--	optional : image, key, title, info, terrain_all
	library.switches = {}

	-- const, as terr-change type key
	library.SWITCH_CHANGE_TYPE = "switch"

	-- switch action variants
	library.switchFuncs = {}

	-- styles for representing switches graphically, <key> - string id
	-- values: OFF is on false 'state', ON - true
	--	terrOff, terrOn - terrain strings, for terrain mode
	--	imgOff, imgOn - images path, for picture mode (later implement)
	--	soundOff, soundOn - sound path (optional) (later implement)
	library.switchAppereances = {}

-- -- --------------- Data I/O --------------------------------
	
	-- add new door and return it's entry
	function library.newDoor(name, door_nodes, cl, reqKey, us, busy)
		local ob = {}
		ob.closed = cl
		ob.usable = us
		ob.key = reqKey
		ob.nodes = door_nodes
		ob.on_busy = busy
		library.doors[name] = ob
		return ob
	end

	-- create and return new terrain appereance mode
	function library.newDoorTerrAppereance(name, off, on)
		local a = {}
		a.terrOff, a.terrOn = off, on
		library.switchAppereances[name] = a
		return a
	end

	-- build door node with one cell; dx,dy - coords, t - closing terrain
	function library.createSingleDoorNode(dx,dy, t)
		local dNodes = {}
		dNodes[1] = {x=dx, y=dy, terr=t}
		return dNodes
	end

	-- add (replace) new switch and return it's entry
	function library.newSwitch(sw_id, sx, sy, action_key, act)
		local sw = {}
		sw.x, sw.y = sx,sy
		sw.actions = {}
		sw.actions[action_key] = act
		library.switches[sw_id] = sw
		return sw
	end

	--	returns existing door key or nil
	function library.existsDoorAt(x,y)
		for k,v in pairs(library.doors) do
			for nk, nv in pairs(v.nodes) do
				if nv.x==x and nv.y==y then
					return k
				end
			end			
		end
		return nil
	end

	--	returns existing door key or nil
	function library.existsSwitchAt(x,y)
		for k,v in pairs(library.switches) do
			if x==v.x and y==v.y then
				return k
			end
		end
		return nil
	end

	--returns door entry or nil
	function library.doorOfPoint(px, py)
		local k = library.existsDoorAt(px,py)
		if k==nil then return nil end
		return library.doors[k]
	end

	--returns switch entry or nil
	function library.switchOfPoint(px,py)
		local k = library.existsSwitchAt(px,py)
		if k==nil then return nil end
		return library.switches[k]
	end

	-- create new simple door (no key) and return it's entry
	-- d_x, d_y - single cell representing door
	-- closed_terr - terrain on close
	--		no actions?
	function library.addSingleCellNoKeyMenuDoor(d_id ,d_x, d_y, closed_terr)
		local ob = {}
		local nd = library.createSingleDoorNode(d_x, d_y, closed_terr)
		ob.nodes = nd
		ob.usable = true
		library.doors[d_id] = ob
		return ob
	end

	-- add (and return it) a switch controling door, door must be existing
	function library.newSwitchBindedToDoor(sw_id, d_id, sw_x, sw_y)
		local act = {}
		act.exec = library.switchFuncs["door_control"]
		local sw = library.newSwitch(sw_id, sw_x, sw_y, "menu", act)
		local door = library.doors[d_id]
		sw.state = door.closed
		sw.door_id = d_id
		return sw
	end

-- -- ------------------------ Utils --------------------------------

	-- returns first found door entry near [px,py] and its key (string id)	
	function library.findAdjacentDoor(px,py)
		local neigh = common.listAdjLocations(px,py)
		local door, name = nil, nil
		for i,p in pairs(neigh) do
			name = library.existsDoorAt(p[1], p[2])
			if name~=nil then
				door = library.doors[name]
				break
			end
		end
		return door, name
	end

-- -- --------------- Binded in eventmacro.cfg --------------------------------
	
	-- returns true if found switch on given unit's pos with "menu" action
	function needShowInteractSwitchMenu(infilt_unit)
		local ux, uy = infilt_unit.x, infilt_unit.y
		local found_p = false
		for k,v in pairs(library.switches) do
			if common.samePoint(v.x, v.y, ux, uy) then
				found_p = true
				break
			end
		end
		if found_p then
			local sw = library.switchOfPoint(ux,uy)
			m_act = sw.actions["menu"]
			if m_act==nil then return false end
			m_filt = m_act.filter
			if m_filt==nil then return true end
			local acc = wesnoth.match_unit(infilt_unit, m_filt)
			return acc
		end
		return false
	end

	-- returns true on found door at given unit's pos and it's passing filter (if specified) 
	function needShowInteractDoorMenu(infilt_unit)
		local ux, uy = infilt_unit.x, infilt_unit.y
		local door, dr_name = library.findAdjacentDoor(ux,uy)
		local acc = false
		-- check door existance
		if door==nil then
			return false
		end
		-- check usable
		if not door.usable then
			return false
		end
		-- check filter
		if door.filter==nil then 
			acc = true
		else
			acc = wesnoth.match_unit(infilt_unit, door.filter)
		end
		-- check key precense (if required)
		if not common.emptyStr(door.key) then
			acc = common.unitCarryThing(infilt_unit, door.key)
		end
		return acc
	end

	-- called when unit tried to click switch (via menu) on it's terrain
	-- filter not finished
	function library.switchClicked(sx, sy)
		local sw = library.switchOfPoint(sx,sy)
		if sw==nil then
			common.warn("Tried to click switch at ["..sx..","..sy.."], which doesn't exist.")
			return
		end
		local s_men = sw.actions.menu
		if s_men==nil then
			common.warn("Tried to click switch at ["..sx..","..sy.."], which doesn't have 'menu' action.")
			return
		end
		--if s_men.filter==nil then			
		--end
		library.accessSwitch(sw, "menu")
	end

	-- called when unit tried to access door (via menu) on adjacent terrain
	function library.doorActivated(dx, dy)
		local dr, dk = library.findAdjacentDoor(dx,dy)
		if dr==nil then
			common.warn("Door near ["..dx..","..dy.."] not found.")
			return
		end
		library.invertDoor(dk)
	end

	-- called on enter/exit hex
	--	x,y - coords
	--	event - "enter_hex" or "exit_hex"
	function library.switchCellStep(x, y, event)
	end

-- -- --------------- Invariants --------------------------------

	-- call visitor function for each door node.
	--	name - door id
	--	visitor - function(x,y, terr)
	function library.visitDoorNodes(name, visitor)
		local n = library.doors[name].nodes
		for k,v in pairs(n) do
			visitor(v.x, v.y, v.terr)
		end
	end

	-- restore terrains and set state to opened
	function library.openDoor(ent, name)
		--local ent = library.doors[name]
		if not ent.closed then return end
		local vis_f = function (nx, ny, nt)
			common.terrChangeRestore(nx, ny)
		end
		library.visitDoorNodes(name, vis_f)
		ent.closed = false
	end

	-- try replace terrains and set state to closed, returns true if succeed, false if at least one on_busy() call returned false
	function library.closeDoor(ent, name)
		--local ent = library.doors[name]
		if ent.closed then return end
		local deciede = {}
		local visit = function(vx, vy, vterr)
			local un_li = wesnoth.get_units({x=vx, y=vy})
			if not common.emptyArray(un_li) then			
				local un = un_li[1]
				local may_cl = ent.on_busy(ent, un)
				table.insert(deciede, may_cl)
			end
		end
		library.visitDoorNodes(name, visit)
		local dec = true
		for k,v in pairs(deciede) do
			if not v then
				dec = false
				break
			end
		end
		if dec then
			local closer = function(px, py, pt)
				common.terrChange(px,py, name, pt, library.DOOR_CHANGE_TYPE, true)
			end
			library.visitDoorNodes(name, closer)
			ent.closed = true
		end
		return dec
	end

	-- open or try to close door scecified by name
	function library.invertDoor(d_name)
		local d = library.doors[d_name]
		if d==nil then
			common.warn("Tried to invert state of unexisting door '"..d_name.."'.")
			return
		end
		if d.closed then
			library.openDoor(d, d_name)
			return true
		else
			local cl = library.closeDoor(d, d_name)
			return cl
		end
	end

	-- change terrains/images depending on switch current state
	-- not finished (images)
	function library.refreshSwitchState(sw, terr_change_id)
		local app_name = sw.appereance
		if common.emptyStr(app_name) then
			common.warn("Switch hasn't appereance.")
			return
		end
		local app = library.switchAppereances[app_name]
		if app==nil then
			common.warn("Unexisting appereance <"..app_name.."> binded to switch.")
			return
		end
		local terr = ""
		if sw.state then
			terr = app.terrOff
		else
			terr = app.terrOn
		end
		common.terrChange(sw.x,sw.y, terr_change_id, terr, library.DOOR_CHANGE_TYPE, true)
	end

	-- calls binded function of switch action and refreshes state
	function library.accessSwitch(sw, action_name)
		local act = sw.actions[action_name]
		if act==nil then
			-- mb common.warn("")
			return
		end
		local res = act.exec(sw)
		if res then
			library.refreshSwitchState(sw, "sw_"..sw.x.."_"..sw.y)
		end
	end

-- -- --------------- Variants --------------------------------
	
	-- no effect on unit and door can't close
	library.doorBusyFuncs["light"] = function(d_ent, un)
		return (un==nil)
	end

	-- kills stuck unit(s) and allow close
	-- not finished
	library.doorBusyFuncs["kill"] = function(d_ent, un)
		--kill
		return true
	end

	-- for switches which provide access to door
	-- returns false if closing failed
	library.switchFuncs["door_control"] = function(s_ent)
		local succ = library.invertDoor(s_ent.door_id)		
		local dr = library.doors[s_ent.door_id]
		s_ent.state = dr.closed
		return succ
	end

	-- switch that creates ally unit
	-- not finished
	library.switchFuncs["summon_ally"] = function(s_ent)
	end

-- -------------------- Init --------------------------

	function library.initAppereances()
		library.newDoorTerrAppereance("orcish_keep", "Co", "Coa")
		library.newDoorTerrAppereance("castle_fire", "Ce^Eb", "Ce^Ebn")
	end

-- ---------------Misc

	function library.test()
		local d1 = library.addSingleCellNoKeyMenuDoor("door1", 9, 5, "Xos")
		d1.on_busy = library.doorBusyFuncs["light"]
		local d2_nodes = library.createSingleDoorNode(10, 7, "Xos")
		local d2 = library.newDoor("door2", d2_nodes, true, nil, false, library.doorBusyFuncs["light"])
		local sw1 = library.newSwitchBindedToDoor("sw1", "door2", 13, 3)
		sw1.appereance = "orcish_keep"
		library.refreshSwitchState(sw1, "sw_13_3")
	end

-- ----------- /DoorSwitch ---------------------

return library
