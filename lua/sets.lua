local library = {}

	function library.defaultComparator(a,b)
		return (a==b)
	end

	-- is elem belong to set	
	function library.inSet(elem, set, comp)
		for k,v in pairs(set) do
			if comp(elem, v) then return true end
		end
		return false
	end

	--	returns array of all unique from set1
	--	set1, set2 - tables
	--	comparator - function(a,b):bool, return true if a and b are the same
	function library.difference(set1, set2, comparator)
		local ret = {}
		for k,v in pairs(set1) do
			if not library.inSet(v, set2, comparator) then
				table.insert(ret,v)
			end
		end
		return ret
	end


return library