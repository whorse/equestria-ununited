local library = {}

local common = wesnoth.require("~add-ons/equestria-ununited/lua/common.lua")
local taggen = wesnoth.require("~add-ons/equestria-ununited/lua/wml_tag_gen.lua")
local setsapi = wesnoth.require("~add-ons/equestria-ununited/lua/sets.lua")

--Common notes:
--	all bounds and restriction checking will be sitied in move_func(), including call of clash_func()
--	pay attention on obj.angle and obj.shape coherence


--TODO:
--	mov_objs destroying (may be used in clashes)
--	Optimize dashit!
--	mmdeia - images, sounds
--	help info
--	better initializing tools, mb return to types generalization
--	auto-stops
--	fix state additional-param(s) passing
--	extend variants: non-vehicles(elemental), killable clash, faster mover, mb immediate start, inertia momentum, things carry/clash
--	implement cache
--	use filters: driver, clash policy

	-- moving objects table, key = unique id
	--	values:
	--		id - string, must be the same as key
	--		x,y - position (base)
	--		(obsolete) type - type key
	--		shape - shape object (table with direction keys), values - table with items:
	--			terr - terrain string
	--			control (optional) - bool, detects control-point(s) for vehicles
	--			x,y (optional) - node actual pos; may be useful as cache somewhere
	--		angle - [n..nw] - means current direction of object, used for detect moving direction
	--		state - string, moving status for now
	--		(not implemented yet) state_ex - table, for additional state variables
	--		move_func - function(mov_obj) : x,y, rotate; returns new(next) object position (3 values)
	--			x,y - point to move to
	--			r - rotate angle, int. each unit means 60* clockwise. 0 - no rotate, negative value - opposite direction
	--		clash_func - function(mov_obj, x,y,r) : bool; must return moving possibility. params:
	--			mov_obj - moving_object entry
	--			x,y,r - new pos and angle to test
	--		menu_func - function(mov_obj) : void function, which fires menu and do some input, usually it must modify mov_obj.state
	--		rear_filter - function(mov_obj, x, y): bool, processing only single cells, return true if [x,y] proper for riding
	--		event_flags - table, with event-named keys and bool values. when true, object activated on event
	--		(not implemented yet) auto_stops - table, keys - state labels, values - ints, as limit of iterations before auto-stop
	--		driver_filter (optional)
	--		pos_cache = array of tables
	--			abs_x, abs_y
	--			pos: table
	--				k: path string, v: {x,y}
	library.instances = {}

	-- moving object types table, key = unique id
	--	values:
	--		
	--		move_style - teleport/fly/ride
	--		move_steps - [turn_start, turn_end, side_turn, ...]
	--library.types = {}

	-- variants collections
	library.rearFilters = {}
	library.moveFuncs = {}
	library.clashFuncs = {}
	library.menuFuncs = {}

	-- const used as id label in common terrain change
	library.TERR_CHANGE_TYPE = "mv_ter"

	-- external name of this lib for providing feedback
	library.LIB_NAME = "moveterrlib"

	--  transitional data storage
	library.cache_enabled = false

-- -- --------------- Data I/O --------------------------------

	--function library.replaceShapeItemValue(sid, key, nv)
		--local sh = common.shapeAreas[sid]
		--sh[key] = nv
	--end

	-- create and return shape obj
	--	s_nodes - array of side patches (direction str)
	--	terrain - terr str to assign values
	--	ctrl - string key of control node, may be nil
	--	ctrl_terr - terrain of control node, may be nil
	function library.createShape(s_nodes, terrain, ctrl, ctrl_terr)
		local ns = {}		
		for k,v in pairs(s_nodes) do
			local new = {}
			ns[v] = new
			if v~=ctrl then
				new.terr = terrain
			else
				new.terr = ctrl_terr
				new.control = true
			end
		end
		return ns
	end

	-- create copy of shape sh, if 'copy_val==false' then values of copy will be bool
	function library.copyShape(sh, copy_val)
		local ns = {}
		for k,v in pairs(sh) do
			local val = true
			if copy_val then val = v end
			ns[k] = val
		end
		return sh
	end

	-- detect is control node on [x,y] now
	function library.hasControlPointAt(key, x,y)
		local mo = library.instances[key]
		local sp = common.shapeActualPositions(mo.shape, mo.x, mo.y)
		for k,v in pairs(sp) do
			if common.samePoint(x,y, v.x, v.y) then
				if mo.shape[k].control then
					return true
				end
			end
		end
		return false
	end

	-- return mov_obj key if ctrl point found at [x,y], else return nil
	function library.findMovObjCtrlAt(x,y)
		for k,v in pairs(library.instances) do
			if library.hasControlPointAt(k, x, y) then return k end
		end
		return nil
	end

	--function library.getObjectRearFilter(obj)
	--	return library.types[obj.type].rear_filter
	--end

-- -- ------------------------ Utils --------------------------------

	-- create terrain-type filter	
	function library.buildTerrainRearFilter(terr)
		local filt = function(mov_obj, x, y)
			return wenoth.match_location(x,y, {terrain=terr})
		end
		return filt
	end

	-- return array of tables
	--	values: 
	--		x,y - trouble cell
	--		trouble - unit/mov_obj
	function checkNewPosition(shape, px, py)
	end

	-- check map bounds and rear_filter restriction for cell
	--	f - rear_filter function
	--	cx,cy - position to check
	--	obj - mov_obj entry
	function library.reachableCell(f, cx, cy, obj)
		--if not common.inMapBounds(cx,cy) then return false end
		return f(obj, cx,cy)
	end

	-- check map bounds and rear_filter restriction for shape with {x,y} values
	--	f - rear_filter function
	--	new_sh - shape with table values as points {x,y} represents actual positions
	--	obj - mov_obj to check for
	function library.reachablePosition(f, new_sh, obj)
		for k,v in pairs(new_sh) do
			if not f(obj, v.x, v.y) then
				return false
			end
		end
		return true
	end

	-- check if new pos reachable, namely map bounds and rear
	function library.canMoveTo(obj, x,y)
		local ns = common.shapeActualPositions(obj.shape, x, y)
		local fi = obj.rear_filter
		return library.reachablePosition(fi, ns, obj)
	end

	-- check if new shape pos is reachable
	--	obj - moving object
	--	r - int, angle
	function library.canRotate(obj, r)
		local rs = common.shapeRotated(obj.shape, r, false)
		local new_pos = common.shapeActualPositions(rs, obj.x, obj.y)
		local fi = obj.rear_filter
		return library.reachablePosition(fi, new_pos, obj)		
	end
	
	-- clear stored coordinates (x,y) from shape values
	function library.eraseShapeXY(sh)
		for k,v in pairs(sh) do
			v.x, v.y = nil, nil
		end
	end

	-- fire WML menu with lua callbacks
	-- almost similar as common.fireMenuLuaCalls() but auto-fill prefixes for call list
	function library.fireMenuAutoPrefix(ask, answers, calls, call_prefix, cancel)
		local cls, cl_str = common.enumToArray(calls, ';'), ""
		for i=1, #cls do
			cl_str = cl_str..call_prefix..cls[i]..";"
		end
		cl_str = string.sub(cl_str, 1, string.len(cl_str)-1)
		common.fireMenuLuaCalls(ask, answers, cl_str, cancel)
	end

	-- return filter struct that matches all units or locations of given shape
	--		params: bx,by - base point
	-- seems to be slow, could be optimized later
	-- not tested
	function library.buildOccupiedFilter(shape, bx, by)
		local arr = {}
		local sh = common.shapeActualPositions(shape, bx, by)
		for k,v in pairs(sh) do
			table.insert(arr,v)
		end
		return taggen.makeMultiplePointsFilter(arr)
	end

	-- return array of units found on area that occupied by shape on given point [bx,by]
	function library.listCapturedUnits(shape, bx, by)
		return wesnoth.get_units(library.buildOccupiedFilter(shape, bx, by))		
	end

	-- only units that belongs to NEW pos and does not belong to OLD pos (e.g. not inside now)
	-- params:	ul_inner - list of inner units, shape_new,nx,ny - new position determiners
	-- returns array of units (userdata) or empty array
	function library.listOnlyClashedOuterUnits(ul_inner, shape_new, nx, ny)
		local un_cmp = function(u1,u2)
			return u1.id==u2.id
		end
		--local ul_inner = library.listCapturedUnits(shape_old, ox, oy)
		local ul_outer = library.listCapturedUnits(shape_new, nx, ny)
		return setsapi.difference(ul_outer, ul_inner, un_cmp)		
	end

	--
	-- too bad...
	function library.findKeyFromShapeXY(sh_xy, x,y)
		for k,v in pairs(sh_xy) do
			if common.samePoint(x,y, v.x, v.y) then
				return k
			end
		end
		return nil
	end

	-- move all units
	--	ul - array of unit userdata
	--	nx,ny - relative offset
	-- not safe as seemed
	function library.moveUnits(ul, nx, ny)
		--local n
		for k,v in pairs(ul) do
			local x,y = v.x+nx, v.y+ny
			common.moveUnit(v, x,y)
		end
	end

-- -- --------------- Invariants --------------------------------

	-- builder of filters which detect changed terrains by given 'id' and 'type'
	function library.buildRestoreTerrFilter(obj_id)
		local f = function(x,y, ch_entry)
			return ch_entry.type==library.TERR_CHANGE_TYPE and ch_entry.id==obj_id			
		end
		return f
	end

	-- erase terrain changes related to given mov_obj and execise new changes depending on current position
	-- not tested
	function library.redisplayObject(m_obj)
		common.terrChangeRestoreByFilter(library.buildRestoreTerrFilter(m_obj.id))		
		local pos = common.shapeActualPositions(m_obj.shape, m_obj.x, m_obj.y)
		for k,v in pairs(pos) do
			local terr = m_obj.shape[k].terr
			common.terrChange(v.x, v.y, m_obj.id, terr, library.TERR_CHANGE_TYPE, true)
		end		
	end

	-- basic function executing terr_object moving
	function library.movingIteration(obj)
		local x,y,r = obj.move_func(obj)
		if r==0 then
			if common.samePoint(x,y, obj.x, obj.y) then
				--wesnoth.message(string.format("no changed pos %s,%s", x,y))
				return				
			end
		else
			local rot_sh = common.shapeRotated(obj.shape, r, true)
			obj.shape = rot_sh
			obj.angle = common.translateDirection(obj.angle, r)
		end
		obj.x, obj.y = x,y
		--library.eraseShapeXY(obj.shape)
		library.redisplayObject(obj)
	end

-- -- --------------- Variants --------------------------------
	-- -- ----- moves ----------

	-- can't violate map bounds
	library.moveFuncs["pixel_step"] = function(mov_obj)
		local x,y,r = mov_obj.x, mov_obj.y, 0
		if mov_obj.state=="stop" then
			--wesnoth.message("stop state")
			return x,y,r			
		end
		local s1,s6 = string.find(mov_obj.state, "rotate")
		local st_rot = false
		if s1==1 and s6==6 then
			local n = tonumber(string.sub(mov_obj.state, 7))
			if library.canRotate(mov_obj, n) then
				r = n
				st_rot = true
			end
		elseif mov_obj.state=="move" then
			x,y = common.whatAdjacentXY(x,y, string.upper(mov_obj.angle))
			--wesnoth.message(string.format("move section. x,y=%s,%s  obj.xy=%s,%s;", x,y,mov_obj.x,mov_obj.y))
			if not library.canMoveTo(mov_obj, x, y) then
				--wesnoth.message(string.format("canMoveTo fail %s,%s", x,y))
				x,y = mov_obj.x, mov_obj.y
			end
		else
			return x,y,r
		end
		if not common.inMapBounds(x,y) then return mov_obj.x, mov_obj.y, 0 end
		local can_move = mov_obj.clash_func(mov_obj, x,y,r)
		if not can_move then return mov_obj.x, mov_obj.y, 0 end
		if st_rot then mov_obj.state="stop" end
		return x,y,r
	end

	-- no clashes
	library.moveFuncs["fly"] = function(mov_obj)
		
	end

	-- swim vehicle
	library.moveFuncs["swim_1"] = function(mov_obj)
		
	end

	-- -- ----- clashes ----------

	-- kill outer units, broke outer vehicle, move inner units
	library.clashFuncs["heavy_vehicle"] = function(obj, x,y,r)
	end

	-- refuse moving if unit clash and to carry onboard units, but totaly ignore other moving_objects
	-- very bad
	library.clashFuncs["simplest_vehicle"] = function(obj, x,y,r)
		if common.sameTriple(x,y,r, obj.x, obj.y, 0) then
			--wesnoth.message("same triple")
			return false
		end		
		local sh = obj.shape
		if r~=0 then sh = common.shapeRotated(obj.shape, r, true) end
		local ul_inner = library.listCapturedUnits(obj.shape, obj.x, obj.y)
		local ul_only_outer = library.listOnlyClashedOuterUnits(ul_inner, sh,x,y)
		if #ul_only_outer ~= 0 then
			return false 
		end
		if #ul_inner == 0 then 
			--common.debugInterruptable("exited no inner units")
			return true 
		end
		local sh_pos = common.shapeActualPositions(sh, x,y)
		local old_pos = common.shapeActualPositions(obj.shape, obj.x, obj.y)
		for k,v in pairs(ul_inner) do
			local nx,ny
			local old_k = library.findKeyFromShapeXY(old_pos, v.x, v.y)
			if r~=0 then							
				local new_k = common.translateDirectionSequence(old_k, r)
				nx,ny = sh_pos[new_k].x, sh_pos[new_k].y
			else
				nx,ny = sh_pos[old_k].x, sh_pos[old_k].y
			end
			common.moveUnit(v, nx,ny)
		end
		return true
	end
	
	-- -- ----- menus ----------

	-- default menu with 5 rotation options, move, stop, and info
	-- not tested
	library.menuFuncs["default"] = function(obj)
		local calls = string.format("rot(nil,%q);copystr('move',%q);copystr('stop',%q);help(%q)", obj.id, obj.id, obj.id, obj.id)
		local prefix = library.LIB_NAME..".menuFuncs.default_"
		library.fireMenuAutoPrefix("Vehicle control:", "Rotate,Accelerate,Stop,Help info", calls, prefix, true)
	end

	-- sub of 'default'
	library.menuFuncs["default_rot"] = function(val, obj_id)
		if val==nil then
			local pref = library.LIB_NAME..".menuFuncs.default_rot"
			local args = string.format("(1,%q);(2,%q);(3,%q);(-1,%q);(-2,%q);", obj_id, obj_id, obj_id, obj_id, obj_id)
			library.fireMenuAutoPrefix("Choose rotate value, [+] is clockwise, [-] is counter clockwise:", "+60,+120,180,-60,-120", args, pref, true)
		else
			if val>=-2 and val<=3 and val~=0 then
				local obj = library.instances[obj_id]
				obj.state = "rotate"..val
			end
		end
	end

	-- sub for 'default'
	library.menuFuncs["default_copystr"] = function(val, obj_id)
		library.instances[obj_id].state = val
	end

	-- sub for 'default'
	-- not finished
	library.menuFuncs["default_help"] = function(obj_id)
	end

	library.menuFuncs["quick_start_with_strafes"] = function(obj)
		local menu_texts = "Accelerate,Rotate,Strafe step,Stop,Help info"
		local prefix = library.LIB_NAME..".quick_start_with_strafes_impl"
		local args = string.format("('start',%q);('rotate',%q)('strafe',%q)('stop',%q)('help',%q)",obj.id, obj.id, obj.id, obj.id, obj.id)
		library.fireMenuAutoPrefix("Vehicle control:",menu_texts,args,prefix, true)
	end

	library.menuFuncs["quick_start_with_strafes_impl"] = function(val, obj_id)
		if val=="start" then
		elseif val=="rotate" then
			library.menuFuncs.default_rot.(nil, obj_id)
		elseif val=="strafe" then
		elseif val=="stop" then
			library.instances[obj_id].state = val
		elseif val=="help" then
		end
	end



-- -------------------- Init --------------------------
	
	--function library.initTypes()
		--local plane = {}
		--plane.move_style = "fly"
		--library.types["flying_vehicle"] = plane
	--end

	-- not finished
	function library.newSeaShip(x,y, id, shape, ev_flags, shallow, swamp)
		local ro = {}
		ro.shape = shape
		ro.event_flags = ev_flags
		ro.state = "stop"
		--ro.angle = "n"
		--ro.clash_func = library.clashFuncs["simplest_vehicle"]
		--ro.move_func = library.moveFuncs["pixel_step"]
		--ro.menu_func = library.menuFuncs["default"]
		ro.id = id
		ro.x, ro.y = x,y
		local rf = "Wo*"
		if shallow then
			-- TODO: test layers policy
			rf=rf..",Wot,Wwg,Ww"
		end
		if swamp then
			rf=rf..",Ss,Sm"
		end
		ro.rear_filter = library.buildTerrainRearFilter(rf)
	end

-- -- --------------- Feedback (WML) --------------------------------

	-- menu filter - global scope
	-- TODO: objects without menu: false
	function doShowTerrMovingMenu(un)
		local mo_key = library.findMovObjCtrlAt(un.x, un.y)
		if mo_key==nil then return false end
		local mvo = library.instances[mo_key]
		if mvo.driver_filter~=nil then
			return wesnoth.match_unit(un, mvo.driver_filter)
		end
		return true
	end

	-- menu responser
	function library.menuActivated()
		local x,y = common.getCurrentCoordinates()
		local k = library.findMovObjCtrlAt(x, y)
		if k==nil then
			common.warn("Failed to match mov_obj after activating menu.")
			return
		end
		local ob = library.instances[k]
		ob.menu_func(ob)
	end

	-- event responser
	function library.eventRaised(ev)
		local movs = {}
		for k,v in pairs(library.instances) do
			if v.event_flags[ev] then
				table.insert(movs, k)
			end
		end
		for i=1, #movs do
			library.movingIteration(library.instances[movs[i]])
		end
	end

-- -- --------------- Test --------------------------------

	function library.newTestObj(x,y)
		local ro = {}
		local sh = library.createShape({"n","ne","nw","n_n","n_n_n"}, "Ce", "n", "Ko")
		ro.shape = sh
		ro.state = "stop"
		ro.angle = "n"
		ro.clash_func = library.clashFuncs["simplest_vehicle"]
		ro.move_func = library.moveFuncs["pixel_step"]
		ro.menu_func = library.menuFuncs["default"]
		ro.id = "mo_test"
		ro.x, ro.y = x,y
		ro.rear_filter = function(o, x,y)
			return true
		end
		ro.event_flags = {["side turn end"] = true}
		library.instances["mo_test"] = ro
		library.redisplayObject(ro)
	end

	function library.test()
		library.newTestObj(10,6)
	end

return library