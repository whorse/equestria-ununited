local library = {}

-- TODO
--   Tests/fix:
--     Things
--     Pilot-rq state switches
--     StinkOut
--   Append:
--     Inner unit restriction system
--   Remove
--     obsolete stuff

-- -- ========================= Common =========================

local common = wesnoth.require("~add-ons/equestria-ununited/lua/common.lua")

-- NOT finished
-- detects if unit could ride transport
function library.canBeTransported(unit_struct)
	if unit_struct.canrecruit then
		return false
	end
	return true
end

-- ______________________________________________________________________________________________
-- -- --------------- Extended moving -- -- ---------------

function library.moveSideExtendedTransport(side_n)
	local ulist = wesnoth.get_units {side = side_n,ability="transp_extended_move"}
end

-- ========================= Terrain objects moving =========================

-- string-key table. contains: exec
-- exec() impl do not need params verify
library.tmSupertypes = {}

-- string-key table.
-- contains: rear_filter, 
--		obj_filter, 
--		clash, 
--		type, 
--		clash_continue
library.tmTypes = {}

-- array of tables
-- item contains: kind,
--		x,y,
--		terr,
--		//nodes(optional),
--		events
--		shape (instead of nodes)
--	parent properties must be copied :
library.tmItems = {}

-- string-key table. there are 2 kinds of functions possible:
-- clash_function params: obj - item, cl_info{kind,px,py,ox,oy} - item body
-- clash_continue function params: obj - item, cl_info_list - array of clash info
library.tmClashes = {}

-- key - shape name
-- value - table(array) of {key = path (relative path from root, example: "n_ne", "se"), value = {terr - string, icon(optional)}}
-- must be moved to Common
library.tmShapes = {}

		----------- types i/o -------------------------

function library.getTmItemSuperKey(id)
	return library.tmTypes[library.tmItems[id].kind].type
end

function library.tmHasEvent(obj, ev)
	for k,v in obj.events do
		if v==ev then return true end
	end
	return false
end

function library.addMovingObj(key, knd, terr,x,y, event_l, shape_name)
	local o = {}
	o.kind, o.terr, o.x, o.y, o.shape = knd,terr,x,y,shape_name
	o.events = event_l
	tmItems[key] = o
	return o
end

-- register new movetype with given supertype
-- params
--	kind_nam - new type name
--	super_key - super object id (tmSypertypes.key)
--	r_terr - [R]ear filter (such as "*^*V*")
-- 	exec_func - function(item_table) returning new X,Y of moving object
-- not finished?
function library.addMovObjType(kind_nam, super_key, r_terr, exec_func)
	local t = {}
	t.rear_filter = library.buildDefaultTerrainNameFilter("rear_terr")
	--t.obj_filter = library.buildDefaultTerrainNameFilter("obj_terr")
	t.rear_terr = r_terr
	--t.obj_terr = o_terr
	t.type = super_key
	t.clash = "standard"
	t.clash_continue = "std_continue"
	--t.canMoveAt = function(nx, ny)		
	--end
	library.tmTypes[kind_nam] = t
	return t
end

function library.addMovObjSupertype(st_name, ev_turn_end, exec_func)
	local ns = {}	
	if exec_func ~= nil then ns.exec = exec_func end
	ns.turn_end = ev_turn_end
	library.tmSupertypes[st_name] = ns
end



		----------- define default MovObj types -------------------------

--library.tmSupertypes.default = {}

library.tmSupertypes["teleport_xy_vehicle"] = {}

-- this vechile instances must have params:
--	active : bool. if true teleport to vechicle.to_x,to_y (this params also must be filled)
--	jump_rear : bool - if false and destanation point is not reachable by Flood Fill - moving will be impossible
library.tmSupertypes["teleport_xy_vehicle"].exec = function(mov_obj_table)
	if not mov_obj_table.active then
		-- no fly
		return mov_obj_table.x, mov_obj_table.y
	end
	return mov_obj_table.to_x, mov_obj_table.to_y
end

library.tmSupertypes["step_direction_vehicle"] = {}

-- this vechile instances must have params:
--	state : idle/n/s/se/sw/ - do fly and it's direction
--	points : int - steps to fly
library.tmSupertypes["step_direction_vehicle"].exec = function(mov_obj_table)
	local obj = tmItems[item_key]
	local kind = tmTypes[obj.kind]
	if obj.variables.active then
		--local can = library.canMoveObject(obj, obj.variables.direction, obj.rear_filter)
		--if can then
		
		--end
		local locset_old = obj.locations;
		
	end	
end

-- default clash: unit-die, block/both/border-no effect
library.tmClashes["standard"] = function(obj, cl_info)
	if cl_info.kind=="unit" then
		-- fire_event=true ??
		wesnoth.fire("kill", {x=cl_info.px, y=cl_info.py})
	end
end

-- clash_continue: do not interrupt if only units on the way
library.tmClashes["std_continue"] = function(obj, cl_info_list)
	for k,v in pairs(cl_info_list) do
		if v.kind~="unit" then return false end
	end
	return true
end

-- register shapes
function library.initShapes()
	local c3 = {}
	c3["n"].terr = "" -- castle
	c3["se"].terr = "" -- castle
	c3["sw"].terr = "" -- castle	
	library.tmShapes["n_se_sw"] = c3		
	library.tmShapes["boat6"] = {
		["s"] = {}, 
		["se"] = {}, 
		["sw"] = {}, 
		["s_se"] = {}, 
		["s_sw"] = {}, 
	}
end

-- not finished
-- register all particular types
function library.initTerrMoves()
	library.addMovObjSupertype("teleport_xy_vehicle", false)
	library.addMovObjSupertype("step_direction_vehicle", false)
	library.addMovObjType("flying_ship")
	--library.addDefaultMoveType("sea ship", "water", "boat")
	--library.addDefaultMoveType("space ship", "space", "boat")
end



-- returns filter function(x,y) which verifies cell by terrain string. check value readed from tmTypes[a_type][field_from]
-- params: a_type: item type (str)
-- field_from: field name (str) to get terrain verify string from.
function library.buildDefaultTerrainNameFilter(a_type, field_from)
	local t = library.tmTypes[a_type]
	local fi = function(x,y)
		return wesnoth.match_loaction(x,y, {terrain = t[field_from]})
	end
	return fi
end

		----------- MovObj utils -------------------------

-- returns true if given pos (one of 6) could provide rear terrains for object root and nodes (if exist)
function library.properMovObjNextPos(mov_obj_key, direction)

end

-- returns true if given pos (one of 6) could provide rear terrains for object root and nodes (if exist)
function library.properMovObjPos(mov_obj_key, px, py)

end

-- if cell occupied by some MovObj now
function library.occupiedMovObjPos(x,y)
end

-- if next cell in given direction already occupied by some another MovObj now
function library.occupiedMovObjNextPos(mov_obj_key, direction)
end

--common.listAllFloodFillGroup(x,y, check_func)

-- returns true if adjacent cell exists and if filter free_cell_filter(x,y):bool passes it
-- dir must be upper case string
function library.canMoveCell(px,py,dir,free_cell_filter)
	local nx,ny = common.whatAdjacentXY(px, py, dir)
	if not common.inMapBounds(nx,ny) then return false end
	return free_cell_filter(nx,ny)
end

-- returns one of [free/block/unit/both/border] values, cell info
function library.tmStandardCellView(x,y, rearFilter)
	local r = "free"
	local unit, terr = nil, nil
	if not common.inMapBounds(x,y) then
		return "bound"
	end 
	if wesnoth.match_location(x,y, {{"filter",{}}}) then
		unit = true
	end
	if not rearFilter(x,y) then
		if unit then
			r="both"
		else
			r="block"
		end
		return r
	end
	if unit then return "unit" end
end

-- dir must be upper case string
-- returns false if no moving troubles or moving result object (mro):
-- 		kind: "free/block/unit/both/border"; ox,oy: object(old) cell; px,py: purpose cell
-- not tested
function library.tmMovingTrouble(x,y,dir, rearFilter)
	local nx,ny = common.whatAdjacentXY(x, y, dir)
	local cv = library.tmStandardCellView(nx,ny, rearFilter)
	if cv=="free" then
		return false
	else
		return {kind=cv, ox=x,oy=y, px=nx,py=ny}
	end
end

-- obj.locations must be filled
-- returns array of clash points or null if there's no one
-- not tested
-- params:
-- dir: upper case direction string
-- obj: movable item info (table)
function library.tmNextMovingTroubleList(obj, direct)
	local func_is_rear = library.tmTypes[obj.kind].rear_filter
	local collectPoints = {}
	local iterator = function(x,y,z)
		local n = library.tmMovingTrouble(x, y, direct, func_is_rear)
		if n then
			table.insert(collectPoints, n)
		end
	end
	obj.locations:iter(iterator)
	if #collectPoints > 0 then 
		return collectPoints
	else
		return nil
	end
end

-- fill item's location variable with locations set of items reachable from x,y (root pos)
function library.writeObjLocations(obj)
	obj.locations = common.listAllFloodFillGroup(obj.x, obj.y, obj.obj_filter)
end

-- mb MP unsafe
-- not finished
-- params:
-- dir: upper case direction string
-- obj: movable item info (table)
function library.canMoveObject(obj,dir,free_cell_filter)
	if obj.locations~=nil then
		local cells = obj.locations:to_pairs()
		local u_dir = string.upper(dir)
		for k,v in pairs(cells) do
			local c = library.canMoveCell(v[1],v[2],u_dir,free_cell_filter)
			if not c then return false end
		end
	else
		-- invalid arg
	end
end

		---------- Proxy functions ----------

-- then redirect it to common replacement util
function library.tmRestoreCell(x,y)
end

function library.tmEditCell(x,y, new)

end

		---------- /Proxy functions ----------

-- shows menu for moving object transport's input
-- returns ...
-- not finished
function library.fireMovObjDriveMenu(move_style)
	common.fireCommonMenu("Vehicle action", "Move,Rotate,Cancel", "driving_answer")
	local an = wesnoth.get_variable("driving_answer")
	if an==nil or an==2 then return nil end
	if an==0 then
		if move_style=="teleport" then

		elseif move_style=="direction" then
			common.fireCommonMenu("Enter moving direction", "n,ne,se,s,sw,nw", "driving_answer")
		end
	elseif an==1 then

	end
	wesnoth.clear_variable("driving_answer")
end

-- obj.locations must be not null
-- mb erase checking for optimize
-- returns location set with new coordinates. terrain store not finished yet
function library.moveTerrainObject(obj,dir)
	local cells = obj.locations:to_pairs()
	local u_dir = string.upper(dir)
	local new_cells = location_set.create()
	for k,v in pairs(cells) do
		--local c = library.canMoveCell(v[1],v[2],u_dir,free_cell_filter)
		--if not c then return false end
		new_cells:insert(whatAdjacentXY(v[1], v[2], u_dir))
	end
	--obj.locations:inter()
	--restore locations which became free
	return new_cells
end

-- check if moving possible then do exec(), else execute clash().
-- not finished
-- params:
-- direct: must be upper case direction str
-- id: type key
-- obj: movable item table
function library.tryMoveObject(id, obj, direct)
	local troub = library.tmNextMovingTroubleList(obj, direct)
	local do_move,abort_move=false,true
	if troub==nil then
		do_move = true
		abort_move = false
	else
		local typ = library.tmTypes[id]
		for k,v in pairs(troub) do
			library.tmClashes[typ.clash](obj, v)
			if v.kind=="bound" then
				abort_move = true
			end
		end
		local cl_c = typ.clash_continue
		if cl_c==nil then return end
		if (not abort_move) then 
			do_move = library.tmClashes[cl_c](obj, troub)
		end
	end
	if do_move then
		library.tmSupertypes[library.getTmItemSuperKey(id)].exec(id)
	end
end

-- main proc which moves all registered objects
function library.tmExecute(ev_name)
	for k,v in pairs(library.tmItems) do
		--local st = library.getTmItemSuperKey(k)
		if library.tmHasEvent(v, ev_name) then
			library.tryMoveObject(k,v)
		end
	end
end

-- -- /Moving objects ---------------------------------------------------
-- ========================= Transport units =========================

-- -- --------------- Shortcuts -- -- ---------------

-- returns some value from transport ability of unit (table) or unit type (string)
function library.getTransVal(src, lastKey)
	return common.quickUnitStaticValue(src, "abilities.transport."..lastKey)
end

function library.nameExtractor(un)
	local cf = un.__cfg
	if cf==nil then
		cf = un
	end
	return common.unitToStr(cf,true,true,true)
end

function library.xyExtractor(un)
	return un.x..","..un.y
end

function library.uidExtractor(un)
	return "'"..un.id.."'"
end

function library.thingToStr(ce)
	return common.getThingNameAt(ce.x, ce.y).." at "..ce.x..";"..ce.y
end

function library.dummyExtractor(un)
	return un
end

-- -- --------------- Build WML -- -- ---------------

-- makes [command][set_variable]
function makeUnitOption(unitstr, varname, setvalue)
	local com = {"command", {
		{"set_variable", {
			name=varname,
			value=setvalue
		}}
	}}
	return common.makeOptionWithCmd(unitstr, com)
end

-- -- --------------- Choice menu launch -- -- ---------------

-- main transport menu (fires [message])
function library.fireRtMenu()
    local un = common.getPrimaryUnit()
	local cmd_out = common.makeCmdLuaCall('trlib.outMenu()');
	local option_out = {"option", {
			message="OUT",
			cmd_out
	}}
	local cmd_in = common.makeCmdLuaCall('trlib.inMenu()')
	local option_in = {"option", {
			message="IN",
			cmd_in
	}}
	local rt_msg = {
		message="Transport menu",
		side_for=wesnoth.current.side,
		option_in,
		option_out,
		common.makeLuaCallOption("View info", "trlib.fireVecInfo()"),
		common.makeCancelOption()
	}
	wesnoth.fire("message", rt_msg)
end


-- -- --------------- Misc -- -- ---------------

-- not tested
function library.enumerateThingsAround()
	local un = getPrimaryUnit()
	local t_list = common.listCellsAround(un.x, un.y)	
	local filtFunc = function(ce)
		return (common.getThingAt(ce.x, ce.y) ~= nil)
	end
	return common.filterArray(t_list, filtFunc)
end

-- finds free acceptable cell for unit (unit_o) nearly transport (tr_unit) and put unit there
function library.putOutedUnit(unit_o, tr_unit)
	local cx, cy = wesnoth.find_vacant_tile(tr_unit.x, tr_unit.y, unit_o)
	unit_o.x, unit_o.y = cx, cy;
	wesnoth.put_unit(unit_o)
end

-- setup transport state: save/restore transport's moves and attack count
-- not tested
function library.setTransportActive(tr_id, val)
	local tru = common.getUnitById(tr_id).__cfg
	local pil_rq = library.getTransVal(tru, "pilot")
	if pil_rq==false then return end
	local ent = library.findTrEntry(tr_id, false)
	if ent==nil then
		common.warn("Transport not found")
		return
	end
	if val then
		tru.moves = ent.moves
		tru.attacks_left = ent.attacks_left
	else
		ent.moves = tru.moves
		ent.attacks_left = tru.attacks_left
		tru.moves = 0
		tru.attacks_left = 0
	end
end

-- -- --------------- Choice menu responds -- -- ---------------

-- fires IN submenu
function library.inMenu()
	common.fireMenuLuaCalls("What to in:", "Pilot,Passanger,Thing", "trlib.inMenuPilot();trlib.inMenuPass();trlib.inMenuThing()", true)
end

-- fires menu enumerating units around transport (primary unit). param callback_name - function name to be called on item select. it must take 2 arguments, X and Y of choosen unit.
function library.inUnitMenuCommon(callback_name)
	local ux, uy = common.getPrimaryUnitPos()
	local units = common.listAdjUnitsCurrSide(ux, uy)
	if (#units == 0) then
		return
	end
	local units2 = common.filterArray(units, library.canBeTransported)
	if (#units2 == 0) then
		return
	end
	local texts = common.enumerateUnitsInfo(units2, library.nameExtractor, ",")
	local params = common.enumerateUnitsInfo(units2, library.xyExtractor, ";")
	common.fireMenuLuaCall("Who come in", texts, "trlib."..callback_name, params, true)
end

-- for quick initialize vars. params (boolean):
-- createEntry - create if not registered, warnIfVoid - alert warning if not registered, sourcePrimary - transport is primary unit (false for secondary)
-- returns userdata, __cfg of primary unit and it's transport entry
function library.getTrVars(createEntry, warnIfVoid, sourcePrimary)
	local tr = nil
	if sourcePrimary then
		tr = common.getPrimaryUnitUserdata()
	else
		tr = common.getSecondaryUnitUserdata()
	end
	local tr2 = tr.__cfg
	local ent = nil
	if createEntry then
		if warnIfVoid then
			ent = library.findTrEntry(tr2.id, false)
			if ent==nil then
				common.warn("Transport not registered")
				ent = library.findTrEntry(tr2.id, true)
			end
		else
			ent = library.findTrEntry(tr2.id, true)
		end
	else
		ent = library.findTrEntry(tr2.id, false)
	end
	return tr, tr2, ent
end

function library.inMenuPilot()
	library.inUnitMenuCommon("execPilotIn")
end

function library.inMenuPass()
	library.inUnitMenuCommon("execPassIn")
end

function library.inMenuThing()
	local tr, tr2, ent = library.getTrVars(true, false, true)
	local places = library.getTransVal(un, "things")
	local busy = 0
	if ent.things ~= nil then
		busy = #ent.things
	end
	if busy > places then
		common.warn("Violated thing limit")
	elseif busy==places then
		wesnoth.message("Cant pick more things")
	else
		local can_pick = library.enumerateThingsAround()
		if #can_pick > 0 then
			common.fireMenuUnitSel(can_pick, "Select thing to take aboard", "trlib.execThingIn", library.thingToStr, library.xyExtractor)
		else
			wesnoth.message("Not found things around")
		end
	end
end

-- fires OUT submenu
function library.outMenu()
	common.fireMenuLuaCalls("What to out:", "Pilot,Passanger,Thing,All units", "trlib.outMenuPilot();trlib.outMenuPass();trlib.outMenuThing();trlib.leaveAllUnits(true)", true)
end

function library.outMenuPilot()
	library.execPilotOut()
end

-- runs passanger select and calls "execPassOut" then
function library.outMenuPass()
	local tr, tr2, ent = library.getTrVars(true, true, true)
	if ent.pass==nil then
		common.warn("No passangers")
	elseif #ent.pass > 0 then
		common.fireMenuUnitSel(ent.pass, "Select unit to out", "trlib.execPassOut", library.nameExtractor, library.uidExtractor)
		--wesnoth.message(#ent.pass)
	end
end

function library.outMenuThing()
	local tr, tr2, ent = library.getTrVars(true, true, true)
	local busy = 0
	if ent.things ~= nil then
		busy = #ent.things
	end
	if busy>0 then
		common.fireMenuUnitSel(ent.things, "Select thing to throw out", "trlib.execThingOut", library.thingToStr, library.dummyExtractor)
	end
end

-- -- --------------- Particular calls (last in menus) -- -- ---------------

-- put unit into primary transport as pilot
function library.execPilotIn(px,py)
	local tr, tr2, ent = library.getTrVars(true, true, true)
	local pil = common.getUnitAt(px,py)
	if ent.pilot == nil then
		wesnoth.extract_unit(pil)
		ent.pilot = pil.__cfg
		ent.pilot.resting = false
		library.setTransportActive(tr2.id, true)
	else
		common.warn("Pilot present")
	end
end

-- not tested
function library.execPassIn(px,py)
	local tr = common.getPrimaryUnitUserdata()
	local tr2 = tr.__cfg
	local upas = common.getUnitAt(px,py)
	local ent = library.findTrEntry(tr2.id, true)
	local max = library.getTransVal(tr2, "units")
	if ent.pass ~= nil then
		local count = #ent.pass
		if count > max then
			common.warn("More then allowed passangers")
		elseif count == max then
			common.warn("All places taken")
		else
			wesnoth.extract_unit(upas)
			ent.pass[count+1] = upas.__cfg
			ent.pass[count+1].resting = false
		end
	end
end

-- get out pilot from primary unit
function library.execPilotOut()
	local tr = common.getPrimaryUnitUserdata()
	local tr2 = tr.__cfg
	local ent = library.findTrEntry(tr2.id, false)
	if ent == nil then
		common.warn("Transport not registered")
		ent = library.findTrEntry(tr2.id, true)
	end
	if ent.pilot == nil then
		common.warn("No pilot")
	else
		--ent.pilot.resting = false
		--library.setTransportActive(tr2.id, false)
		--library.putOutedUnit(ent.pilot, tr2)
		--ent.pilot = nil
		library.leaveTransport(tr2, ent, -1)
	end
end

function library.execPassOut(passId)
	local tr, tr2, ent = library.getTrVars(true, true, true)
	local ind = -1
	for i=1, #ent.pass do
		if ent.pass[i].id == passId then
			ind = i
			break
		end
	end
	if ind ~= -1 then
		-- local u = ent.pass[ind]
		-- table.remove(ent.pass, ind)
		-- u.resting = false
		-- library.putOutedUnit(u, tr2)
		library.leaveTransport(tr2, ent, ind)
	else
		common.warn("Unknown passanger id")
	end
end

function library.execThingIn(tx,ty)
	local tr, tr2, ent = library.getTrVars(true, true, true)
	local last = #ent.things
	ent.things[last+1] = common.getThingAt(tx,ty)
	common.delThingAt(tx,ty)
end

-- not finished
function library.execThingOut(tname)
	local tr, tr2, ent = library.getTrVars(true, true, true)
	local todel = -1
	for i=1, #ent.things do
		if ent.things[i]==tname then
			todel = i
			break
		end
	end
	if todel~=-1 then
		local th = table.remove(ent.things, todel)
		-- need to be remastered
		common.putThingAt(tr2.x, tr2.y, th)
	end
end

-- shows message about transport status (as primary unit)
function library.fireVecInfo()
	local tr, trid = common.getPrimaryUnit()
	local str = "["..common.unitStaticValue(tr.type, "name").."] \n"
	local ent = library.findTrEntry(trid, false)
	local pil, pass, th, rq_pil = "none", "0", "0", "(not required)"
	if ent ~= nil then
		if ent.pilot ~= nil then
			pil = common.unitToStr(ent.pilot, true,true,true)
		end
		if ent.pass ~= nil then
			pass = #ent.pass
		end
		if ent.things ~= nil then
			th = #ent.things
		end
	end
	if common.unitStaticValue(tr.type, "abilities.transport.pilot") then
		rq_pil = "(required)"
	end
	str = str.."Pilot = "..pil.."\n"
	str = str.."Passangers = "..pass.." of "..common.unitStaticValue(tr.type, "abilities.transport.units").."\n"
	str = str.."Things = "..th.." of "..common.unitStaticValue(tr.type, "abilities.transport.things")
	common.fireMessageCurrSide(str)
end

-- make all units to leave transport
-- from_primary - which transport append for, primary or secondary unit (boolean)
function library.leaveAllUnits(from_primary)
	local tr, tr2, ent = library.getTrVars(false, true, from_primary)
	while library.passCount(ent) > 0 do		
		library.leaveTransport(tr2, ent, 1)
	end
	if library.hasPilot(ent) then
		library.leaveTransport(tr2, ent, -1)
	end
end

-- -- --------------- Units storage -- -- ---------------

-- table storing entities inside transport. table key - transport unit id. values are tables too.
-- subtable keys:
-- "pilot"-pilot unit, "pass"-units array, "things"-thing array, "moves"-movepoints buffer, "attacks_left" - attacks buffer
library.insideUnits = {}

-- init and get new transport entry
function library.addTransportEntry(tid)
	local new_entry = {pass={}, things={}}
	library.insideUnits[tid] = new_entry
	return new_entry
end

-- remove transport entry
function library.delTransportEntry(tid)
	library.insideUnits[tid] = nil
end

-- get existing transport entry by id or create it if neccessarry
function library.findTrEntry(trid, create)
	local tr = library.insideUnits[trid];
	if tr == nil then
		if create then
			return library.addTransportEntry(trid)			
		else
			return nil
		end
	else
		return tr;
	end
end

-- make particular passanger to leave transport. params:
-- tr_cfg - unit table (.__cfg), t_ent - transport entry (from insideUnits collection), ind - passanger index (set -1 for pilot leaving)
function library.leaveTransport(tr_cfg, t_ent, ind)
	if ind == -1 then
		t_ent.pilot.resting = false
		library.setTransportActive(tr_cfg.id, false)
		library.putOutedUnit(t_ent.pilot, tr_cfg)
		t_ent.pilot = nil
	else
		local u = t_ent.pass[ind]
		table.remove(t_ent.pass, ind)
		u.resting = false
		library.putOutedUnit(u, tr_cfg)
	end
end

-- -- --------------- Condition check -- -- ---------------

-- check if pilot's place busy
-- en - transport entry
function library.hasPilot(en)
	if en == nil then
		return false
	elseif en.pilot == nil then
		return false
	else
		return true
	end
end

-- returns passanger count
-- en - transport entry
function library.passCount(en)
	--local tr, tr_id = common.getPrimaryUnit()
	--local en = library.findTrEntry(tr_id, false)
	if en == nil then
		return 0
	elseif en.pass == nil then
		return 0
	else
		return #en.pass
	end
end

-- test how much transport unit is loaded. un - unit struct, forUnits - boolean. if false, check about things
-- returns null if transport entry not found. else returns number, how many passanger places is free. negative value means limit violation, so error was previously.
function library.checkBounds(un, forUnits)
	local current, max = 0,0
	local un2 = common.tryGetUnitTable(un)
	local ent = library.findTrEntry(un2.id, false)
	if ent==nil then return nil end
	if forUnits then
		max = library.getTransVal(un2, "units")
		if ent.pass~=nil then
			current = #ent.pass
		end
	else
		max = library.getTransVal(un2, "things")
		if ent.things~=nil then
			current = #ent.things
		end
	end
	return max-current
end

-- -- --------------- Event responce -- -- ---------------

-- key - die_type name (str), value - function to exec
library.dieTypes = {}

function library.onDie()
	local un = common.getPrimaryUnit()
	local dt = library.getTransVal(un, "die_type")
	library.dieTypes[dt]()
	library.delTransportEntry(un.id)
end

-- set unit number property with limit verify
function library.editUnitValue(un, key, edit, limit)
	un[key] = un[key] + edit
	if edit > 0 then
		if un[key] > limit then
			un[key] = limit
		end
	elseif edit < 0 then
		if un[key] < limit then
			un[key] = limit
		end
	end
end

-- restore movement, attacks, handle healing/curse effects
function library.refreshUnit(un)
	un.moves = un.max_moves
	if un.resting then
		if not common.getUnitStatus(un, "unhealable") then
			library.editUnitValue(un, "hitpoints", 2, un.max_hitpoints)
		end
	else
		un.resting = true
	end
	if common.getUnitStatus(un, "poisoned") then		
		library.editUnitValue(un, "hitpoints", -8, 1)
	end
	un.moves = un.max_moves
	un.attacks_left = un.max_attacks
end

-- not tested. prolly must use copy_unit
function library.onSideTurn()
	for trid, stored in pairs(library.insideUnits) do
		if stored.pilot~=nil then
			library.refreshUnit(stored.pilot)
		end
		if #stored.pass>0 then
			for i=1, #stored.pass do
				library.refreshUnit(stored.pass[i])
			end
		end
		-- move to other function
		local un = wesnoth.get_units({ id=trid })[1].__cfg
		local pil_rq = library.getTransVal(un, "pilot")
		stored.moves = un.max_moves
		stored.attacks_left = un.max_attacks
		if (pil_rq and stored.pilot==nil) then
			un.moves = 0
			un.attacks_left = 0
		end
	end
end

-- register transport on recruit
function library.onRecruit()
	local un = common.getPrimaryUnit()
	library.addTransportEntry(un.id)
end

-- -- --------------- DIE TYPES -- -- ---------------
-- functions called on transport death. there are actions calculating fate for units who were inside of killed transport.
-- dead transport is primary unit

-- pilot loses half of hp, passangers -10 points, nobody dies
library.dieTypes["default"] = function()
	local tr, tr2, ent = library.getTrVars(false, true, true)
	if ent==nil then
		common.warn("Unregistered transport died")
		return
	end
	if ent.pilot~=nil then
		library.editUnitValue(ent.pilot, "hitpoints", -math.ceil(ent.pilot.max_hitpoints / 2), 1)
		library.putOutedUnit(ent.pilot, tr2)
	end
	if #ent.pass>0 then
		for i=1, #ent.pass do
			library.editUnitValue(ent.pass[i], "hitpoints", -10, 1)
			library.putOutedUnit(ent.pass[i], tr2)
		end
	end
end

library.dieTypes["all_dead"] = function()
	
end

-- not finished
-- those who can't fly (teleport or etc) gonna die
library.dieTypes["aircraft_crash"] = function()
	local tr, tr2, ent = library.getTrVars(false, true, true)
	if ent.pilot~=nil then
		library.editUnitValue(ent.pilot, "hitpoints", -math.ceil(ent.pilot.max_hitpoints / 2), 1)
		if not library.crashableUnit(ent.pilot) then
			library.putOutedUnit(ent.pilot, tr2)
		end
	end
	if #ent.pass>0 then
		for i=1, #ent.pass do
			if not library.crashableUnit(ent.pass[i]) then
				library.putOutedUnit(ent.pass[i], tr2)
			end
		end
	end
end

-- -- --------------- Misc2 -- -- ---------------

-- detect if unit crashes on falling
-- uncrashable units are flyers, teleporting mages, other protection users, etc.
function library.crashableUnit(un)
	local fly_mts = "pegasusfly,cloudfly,fly"
	-- woodlandfloat,fly,smallfly,lightfly,float,undeadfly,undeadspirit,spirit,drakefly,drakefly2,drakeglide,drakeglide2
	if un.movement_type~=nil then
		if common.presentInList(un.movement_type, fly_mts, ",") then
			return false
		end
	else
		--if common.quickUnitStaticValue(un, "")
	end
	local uncrashable_types = "Unicorn_Assistant,Bug_Queen,Bug_Fighter,Earth_Consul"
	if common.presentInList(un.type, uncrashable_types, ",") then
		return false
	end
	return true
end

function library.needShowTerrMovObjMenu(loca)
end

-- filter function, deciedes to show driving menu or not
function isTransportMovObj(un)
	-- false if no driver unit on cell
	if un == nil then return false end
	local stayOnRoot = false
	local movObj = nil
	for k,v in pairs(library.tmItems) do
		if un.x==v.x and un.y==v.y then
			stayOnRoot = true
			movObj = v
			break
		end
	end
	if not stayOnRoot then return false end
	-- true if supertype of object is one from enumerated, false else
	return common.presentInList(tmTypes[v.kind], "step_direction_vehicle,teleport_xy_vehicle", ",")
end

-- -- ---------------  -- -- ---------------

return library
