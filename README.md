# README 
Pony addon for "Battle for Wesnoth"
All work and testing going on version 1.12.6
# STATUS
At this time addon has pre-beta status. Main era (Hoof and Tail) needs some balance fixes, artwork, scenarios and some other stuff.
Second era (Grand Divide) at this moment is not even WIP, so it's not playable.
# NOTES
After release 1.14.X we moved to this branch, compatibility with 1.12.X will still be tested, but some things may cause bugs and errors.
Mod directory must be named "equestria-ununited"
