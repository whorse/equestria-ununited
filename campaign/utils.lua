local utl = {}

-- GCP bits (higher bits not used): ...UXXXXXXXXYYYYYYYY
utl.GCP_BIT_UNDERGROUND = 131072
utl.GCP_BITS_X = 0xFF00
utl.GCP_BITS_Y = 0xFF

-- translate cell position (x,y, underground) into number key (GCP)
function utl.toGcp(x,y,u)
	local xs = (x << 8) & utl.GCP_BITS_X
	local ys = y & utl.GCP_BITS_Y
	local us = 0
	if u then
		us = utl.GCP_BIT_UNDERGROUND
	end
	return (xs | ys) | us
end

-- decode GCP number into cell coordinates
-- returns 3 values: x,y (numbers 0..255), underground (boolean)
function utl.decodeGcp(key)
	local x = (utl.GCP_BITS_X & key) >> 8
	local y = (utl.GCP_BITS_Y & key)
	local u = false
	if (utl.GCP_BIT_UNDERGROUND & key) ~= 0 then
		u = true
	end
	return x,y,u
end

--------------------------- map utils -----------------------------------------------------

return utl;