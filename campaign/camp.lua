local lib = {}

local common = wesnoth.require("~add-ons/equestria-ununited/lua/common.lua")
local cutil = wesnoth.require("~add-ons/equestria-ununited/campaign/utils.lua")

--------------------------- Data initialize -----------------------------------------------------

-- Init new cell's subtables
function lib.newCellEntry()
	local ce = {}
	ce.doorways = {}
	--ce.elements = {}
	--ce.elements.weather = {}
	--ce.persons = {}
	return ce
end

-- root container stores all game info
local gameData = {}
	-- init subcontainers
	gameData.cells_table = {}
	gameData.elements = {}
	gameData.weather = {}
	gameData.persons = {}
	gameData.base_constraints = {}
	gameData.base_constraints.cells_base = {}
	gameData.base_constraints.persons = {}
	gameData.base_constraints.persons.meta = {}
	gameData.base_constraints.persons.meta.behaviours = {}
	gameData.base_constraints.persons.meta.purposes = {}
	gameData.base_constraints.persons.meta.stereotypes = {}
	gameData.base_constraints.persons.alive_persons = {}

	gameData.base_constraints.teams = {}
	gameData.player = {}
	gameData.player.pos = {}
	gameData.time = 0
	gameData.base_constraints.animals = {}
	gameData.common_knowlege = {}
	gameData.base_constraints.map = {}
	gameData.base_constraints.map.height = 24
	gameData.base_constraints.map.width = 24
local unitList = {}
local behaveFunctions = {}
local purposeFunctions = {}

function lib.registerAnimalType(type_id, name, plural_name, unit_type_id)
	local nt = {}
	nt.type_id = type_id
	nt.name = name
	nt.plural_name = plural_name
	nt.unit_type_id = unit_type_id
	table.insert(gameData.base_constraints.animals, nt)
end

function lib.registerTeam(str_id)
	gameData.base_constraints.teams[str_id] = {}
	gameData.base_constraints.teams[str_id].team_likes = {}
end

function lib.createBlankPersonEntry()
	--local en = {}
	--en.purposes = {}
	--en.knowlege = {}
	--return en
end

function lib.getPersonEntry(pid, create)
	local p = gameData.persons[pid]
	if pid==nil then
		if create then
			gameData.persons[pid] = {}
			return gameData.persons[pid]
		end
	end
	return p
end
		-------------- game specific init -----------------------------------------------------

function lib.initAnimals()
	registerAnimalType("Wolf", "Wolf", "Wolfs", "Wolf")
	registerAnimalType("Rat", "Rat", "Rats", "Giant Rat")
end

-- Register main teams and relations between them
function lib.initTeamData()
	lib.registerTeam("earthborn")
	lib.registerTeam("whitegold")
	lib.registerTeam("moon_star")
	lib.registerTeam("cult")
	lib.registerTeam("stormwing")
	lib.registerTeam("everfree")
	lib.registerTeam("hive")
	-- lib.registerTeam("neutral") --neutral mustn't have a team preferences. at least in the beginning.
	local tm_list = gameData.base_constraints.teams
	tm_list["earthborn"].race_likes = {
		pegasi = 0
		bugs=-5		
	}
	tm_list["earthborn"].team_likes = {
		whitegold = -80,
		moon_star = -90,
		cult = -50,
		stormwing = -20,
		everfree = -50,
		hive = -75,
		neutral = 5
	}
	tm_list["whitegold"].race_likes = {
		earth=4
		pegasi=4
		unicorn=10
		drakes=2
	}
	tm_list["whitegold"].team_likes = {
		earthborn = -60,
		moon_star = -20,
		cult = -40,
		stormwing = -50,
		everfree = -50,
		hive = -30,
		neutral = 10
	}
	--ed
	tm_list["moon_star"].race_likes = {
		earth=4
		pegasi=4
		unicorn=10
		drakes=12
	}
	tm_list["moon_star"].team_likes = {
		whitegold = -60,
		earthborn = -90,
		cult = -70,
		stormwing = -60,
		everfree = -50,
		hive = -75,
		neutral = 1
	}
	tm_list["cult"].team_likes = {
		whitegold = -60,
		earthborn = -65,
		moon_star = -95,
		stormwing = -55,
		everfree = -15,
		hive = -35,
		neutral = -10
	}
	tm_list["stormwing"].race_likes = {
		earth=-10
		unicorn=-10
	}
	tm_list["stormwing"].team_likes = {
		whitegold = -60,
		earthborn = -20,
		moon_star = -80,
		cult = -55,
		everfree = -50,
		hive = -75,
		neutral = -10
	}
		--ed
	tm_list["hive"].race_likes = {
		earth=16
		pegasi=20
		unicorn=20
	}
	tm_list["hive"].team_likes = {
		whitegold = -20,
		earthborn = -45,
		moon_star = -30,
		stormwing = -25,
		everfree = -30,
		cult = -50,
		neutral = -5
	}
	tm_list["fishes"].team_likes = {
		cult = -30,
		hive = -1
	}
	tm_list["drakes"].race_likes = {
		--pony = -10,
		bugs = -5
		pegasi = 4
		unicorn = 0
		griffin = -9
	}
	tm_list["drakes"].team_likes = {
		whitegold = 20,
		moon_star = 10,	
		cult = -10,
		hive = -5,
	}
	-- not racit at all
	tm_list["everfree"].team_likes = {
		whitegold = -70,
		earthborn = -70,
		moon_star = -70,
		stormwing = -70,
		cult = -30,
		hive = -20,
		neutral = 20
	}
end

function lib.initCharacters()
	lib.registerPerson("bug_queen", "Bug_Queen", "Queen Chrysalis", {brave=8, curiousity=6, luck=5}, "hive")
	lib.registerPerson("aj", "Earth_Tribun", "Applejack", {brave=10, curiousity=1, luck=3}, "earthborn")
	lib.registerPerson("a_bloom", "Earth_Filly", "Apple Bloom", {brave=7, curiousity=5, luck=3}, "earthborn")
	lib.registerPerson("ts", "Earth_Tribun", "Twilight Sparkle", {brave=4, curiousity=9, smart=9}, "moon_star")
	lib.registerPerson("rd", "Pegasi_Commander", "Rainbow Dash", {brave=10, curiousity=1, luck=3}, "stormwing")
	lib.registerPerson("fs", "Pegasi_Sergeant", "Fluttershy", {brave=7, curiousity=6, luck=5}, "everfree")
	lib.registerPerson("rara", "Charity", "Rarity", {brave=4, curiousity=8, luck=6}, "whitegold")
	lib.registerPerson("pp", "Laughing_Mare", "Laughing Mare", {curiousity=6, luck=8}, "cult")
end

		------------- meta data -----------------------------------------------------

function lib.initBehaviours()
	local sv_ev_body = {}
	sv_ev_body.event = {name="attack",first_time_only=false}
	sv_ev_body.insert_unit_filter = "second_unit"
	lib.registerBehaviour("save_vip", "event", sv_ev_body)
end

function lib.initPurposes()
	local gt_l =  function(args)
		--registers new candidate_action for moving unit on cell
	end
	local gt_g =  function(args)
		--moves person on next level in shortest path
		--input:
		--args.x,y,underground : current
		--args.person_id : string, person to move
		--output: table.x,y,u - new position
	end
	lib.registerPurpose("go_to", gt_l, gt_g)
end

--------------------------- Data access -----------------------------------------------------

-- Builds a string key which represents a single map location (Global Cell Pointer)
-- deprecated
function lib.gcpOf(x,y,z)
	return cutil.toGcp(x,y,z)
end

-- Extracts x,y (coords) and z (underground flag) from GCP key string
-- deprecated
function lib.decodeGcpKey(gcp_key)
	return cutil.decodeGcp(gcp_key)
end

-- Checks coordinates are inside map bounds
function lib.existCell(x,y)
	if x<=0 then return false end
	if y<=0 then return false end
	local mp = gameData.base_constraints.map
	if x>mp.width then return false end
	if y>mp.height then return false end
	return true
end

-- Access to specific cell entry.
function lib.getCellEntry(x,y,z, create)
	local key = lib.gcpOf(x,y,z)
	local ent = gameData.cells_table[key]
	if ent==nil then
		if create then
			ent = lib.newCellEntry()
			gameData.cells_table[key] = ent
		end
	end
	return ent
end

-- Access to doorways list
function lib.getCellDoorwaysTable(x,y,z)
	local ent = lib.getCellEntry(x,y,z,false)
	if ent~=nil then
		return ent.doorways
	end
end

function lib.setDefaultMap(x,y,z,map_str)
	local ce = lib.getCellEntry(x,y,z, true)
	ce.map = map_str
end

function lib.getDefaultMap(x,y,z)
	local ce = lib.getCellEntry(x,y,z, false)
	if ce==nil then return nil end
	return ce.map
end

lib.directionMap = {["n"]=0,["s"]=180,["ne"]=60,["se"]=120,["nw"]=300,["sw"]=240}

function lib.gameDataToWml(varname)
	--local rt = {""}
end

--------------------------- Environment edit functions -----------------------------------------------------

		------------- purposes and behaviours -----------------------------------------------------

-- Adds new behaviour item. Params:
--  beh_name : string - unique key
--  trigger_type : "event" or "condition" - when behaviour gonna exercise
--  trigger_body : table - {event,insert_unit_filter} or {eval_func,exec_type,exec_name} (depending on trigger_type)
--    ex_type : "wml" or "lua"
--    ex_name : string - function or custom event name (depending on ex_type)
function lib.registerBehaviour(beh_name, trigger_type, trigger_body)
	if trigger_type~="event" and trigger_type~="condition" then
		--wrong input error
		return nil
	end
	local nb = {}
	nb[trigger_type] = trigger_body
	gameData.base_constraints.persons.meta.behaviours[beh_name] = nb
end

function lib.registerPurpose(pname, function_local, function_global)
	purposeFunctions[pname] = {}
	purposeFunctions[pname].localFunc = function_local
	purposeFunctions[pname].globalFunc = function_global
end

		------------- doorways -----------------------------------------------------

-- Edit default doorways of cell (up to 6). Params:
--  gx,gy,gz - cell coordinates
--  doorway_keys - table with direction keys or string with comma-separated direction enumeration
function lib.setDefaultDoorways(gx, gy, gz, doorway_keys)
	local ent = lib.getCellEntry(gx,gy,gz,true).doorways
	local def = {}
	if type(doorway_keys)=="table" then
		for k,v in pairs(lib.directionMap) do
			if doorway_keys[k] then 
				def[k] = true 
			end
		end
	elseif type(doorway_keys)=="string" then
		local arr = common.enumToArr(doorway_keys)
		for i=1, #arr do
			def[arr[i]] = true
		end
	end
	ent.default = def
end

-- Removes fields from doorway which from specified side. Params:
--  doorway - table with direction keys
--  border - string value "up", "down", "left", or "right"
function lib.removeImpossibleDoorwayKeys(doorway, border)
	if border=="up" then
		doorway.n = nil
	elseif border=="down" then
		doorway.s = nil
	elseif border=="left" then
		doorway.nw = nil
		doorway.sw = nil
	elseif border=="right" then
		doorway.ne = nil
		doorway.se = nil
	end
end

-- rem_filter: function(key, value) which returns bool (do remove or not)
function lib.removeDoorways(gx, gy, gz, rem_filter)
	
end

function lib.defaultDoorwayToStr(dw)
end

function lib.defaultDoorwayFromStr(dw_str)
end

		---------------- player -----------------------------------------------------
-- (global)
function lib.setPlayerPos(px,py,pz)
	gameData.player.pos.x = px
	gameData.player.pos.y = py
	gameData.player.pos.underground = pz
end

function lib.setPlayerTeam(new_t)
	gameData.player.team = new_t
end
		---------------- knowlege -----------------------------------------------------

-- Check if knowlege entries are similar
function lib.sameKnowlege(entry1, entry2)
	if entry1.type == entry2.type then
		return entry1.value == entry2.value
	end
	return false
end

-- Add knowlege entry to table unless similar entry was found there
function lib.appendKnowlegeEntry(k_table, entry)
	for k,v in pairs(k_table) do
		if lib.sameKnowlege(entry, v) then return nil end
	end
	table.insert(k_table, entry)
end

		-------------- weather -----------------------------------------------------

-- Detect if weather item does nothing
function lib.isVoidWeather(w)
	local keys = {"rain", "wind_amount", "clouds", "temp"}
	for k,v in pairs(keys) do
		if not common.isNilOrZero(w[v]) then return false end
	end
	return true
end

-- Makes degree value be multiple to 60
-- not tested
function lib.roundDirection(dir_deg)
	--local points = 
	local times = math.floor(dir_deg / 60)
	local rem = dir_deg % 60
	local add = 0
	if rem >= 30 then add = 60 end
	local ret = times*60 + add
	if ret>=360 then ret = ret-360 end
	return ret
end

-- Calculates length of vector
function lib.vectorLen(vx,xy)
	return math.sqrt(vx*vx + vy*vy)
end

-- Returns vector representation of wind params
function lib.windToVector(amount, direction)
	--local dirs = {["ne"]=90, ["se"]=180, ["sw"]=270}
	local angle = lib.directionMap[direction]
	local vx = math.cos(math.rad(angle))*amount
	local vy = math.sin(math.rad(angle))*amount
	return vx,vy
end

-- Converts degree value into string direction
function lib.directionFromDeg(deg)
	for k,v in pairs(lib.directionMap) do
		if v==deg then return k end
	end
	if deg==360 then return "n" end
end

-- Merges 2 vectors, for non-colinear case only
-- not tested
function lib.mergeWind(dir1, dir2, len1, len2)
	local vx1, vy1 = lib.windToVector(len1, dir1)
	local vx2, vy2 = lib.windToVector(len2, dir2)
	local vx3, vy3 = (vx1+vx2), (vy1+vy2)
	local len = lib.vectorLen(vx3,xy3)
	--local cos_a = vx3/len
	local cos_b = vy3/len
	local b_angle = math.deg(math.acos(cos_b))
	return lib.directionFromDeg(lib.roundDirection(b_angle)), len
end

function lib.mergeWeather(base, ext)
	local w = {}
	w.temp = base.temp + ext.temp
	w.rain = ext.rain
end

		---------------- persons -----------------------------------------------------

-- Sets/replaces [unit] entry for given person id
function lib.setUnit(key_str, unit_table)
	unitList[key_str] = unit_table
end

-- Create new person: unit entry and alive_persons entry
function lib.registerPerson(key_str, unit_type_str, unit_name, props_table, team_str)
	local un = wesnoth.create_unit({type=unit_type_str, name=unit_name, canrecruit=true})
	lib.setUnit(key_str, un)
	local per = {}
	per.properties = props_table
	per.team = team_str
	gameData.base_constraints.persons.alive_persons[key_str] = per
end

-- Set person's position (global)
function lib.setPersonPos(pid, x, y, under)
	local p = lib.getPersonEntry(pid, false)
	p.x = x
	p.y = y
	p.underground = under
end

-- Set person's position (local)
function lib.setPersonLocalPos(pid, x, y)
	local u = unitList[pid]
	if u==nil then return end
	u.x = x
	u.y = y
end

-- Set person's purpose table
function lib.setPersonPurpose(pid, pps)
	local p = lib.getPersonEntry(pid, false)
	p.purpose = pps
end
--------------------------- New WML tags -----------------------------------------------------

function lib.registerNewTags()
	wesnoth.wml_actions["set_default_map"] = function(pars)
		lib.setDefaultMap(pars.x, pars.y, pars.underground, pars.map)
	end
	wesnoth.wml_actions["set_player_position"] = function(pars)
		lib.setPlayerPos(pars.x, pars.y, pars.underground)
	end
	wesnoth.wml_actions["set_player_team"] = function(pars)
		lib.setPlayerTeam(pars.value)
	end
	wesnoth.wml_actions["save_campaign_state"] = function(pars)
		wesnoth.set_variable("CAMPAIGN_GAME_DATA", common.luaToWml(gameData))
	end
	wesnoth.wml_actions["restore_campaign_state"] = function(pars)
		gameData = common.wmlToLua(wesnoth.get_variable("CAMPAIGN_GAME_DATA"))
	end
	wesnoth.wml_actions["set_player_side_info"] = function(pars)
		--local side_n = common.findSideByTeamName("player")
		--wesnoth.sides[side_n].team_name = gameData.player.team
		--wesnoth.get_sides({team_name="player"})[1].team_name = "SHIT"
		--wesnoth.put_unit(unitList["player"])
		wesnoth.fire("modify_side", {{"filter_side",{team_name="player"}},team_name=gameData.player.team})
		wesnoth.put_unit(wesnoth.get_variable("players_unit"))		
	end
end

--------------------------- World state change  -----------------------------------------------------

--function lib.worldNextWeather()
--	local tempWeath = {}
--	for k,v in pairs(gameData.cells_table) do
--		if v.elements ~= nil then
--			if v.elements.weather ~= nil then
				-- ...
--			end
--		end
--	end
--end

--function lib.worldNextState()
	
--end

---------------------------  -----------------------------------------------------

lib.registerNewTags()

return lib
